﻿using UnityEngine;
using System.Collections;
using UnityLibrary.Extensions;

namespace FigureDraw.Logic
{
    /// <summary>
    /// Create grid on display
    /// </summary>
    public class Grid : MonoBehaviour
    {
        #region Fields
        /// <summary>
        /// Line prefab
        /// </summary>
        [SerializeField] private GameObject linePrefab = null;
        
        /// <summary>
        /// Distanse between two lines
        /// </summary>
        [SerializeField] private int distance = 1;
        
        /// <summary>
        /// Count of lines to one direction
        /// </summary>
        [SerializeField] private int size = 20;
        #endregion

        #region Unity Events
        private void Awake()
        {
            this.Check("linePrefab", linePrefab != null, LogType.Error);
        }

        /// <summary>
        /// Creating grid on start
        /// </summary>
        private void Start()
        {
            for (int i = 0; i < size; i++)
            {
                var line = Instantiate(linePrefab, new Vector3(distance * i, 0f, 0f), Quaternion.identity) as GameObject;
                line.transform.SetParent(transform);
                if (i != 0)
                {
                    line = Instantiate(linePrefab, new Vector3(-distance * i, 0f, 0f), Quaternion.identity) as GameObject;
                    line.transform.SetParent(transform);
                }
                line = Instantiate(linePrefab, new Vector3(0f, distance * i, 0f), Quaternion.Euler(0f,0f,90f)) as GameObject;
                line.transform.SetParent(transform);
                if (i != 0)
                {
                    line = Instantiate(linePrefab, new Vector3(0f, -distance * i, 0f), Quaternion.Euler(0f, 0f, 90f)) as GameObject;
                    line.transform.SetParent(transform);
                }
            }
            
        }
        #endregion
    }


}
