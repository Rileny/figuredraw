﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FigureDraw.Logic.ContourAnalysis;
namespace FigureDraw.Logic
{
    /// <summary>
    /// Class that we saving
    /// </summary>
    [Serializable]
    public class Templates 
    {
        public readonly List<Template> templatesList=new List<Template>();

        public Templates(IEnumerable<Template> templates)
        {
            templatesList = templates.ToList();
        }

        public Templates()
        {
            
        }
    }


}
