﻿using UnityLibrary.Services;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace FigureDraw.Logic
{
    /// <summary>
    /// Service for save/load templates
    /// </summary>
    public class LoadSaveTemplates: IService 
    {
        public void Save(Templates templates)
        {
            var serializator=new XmlSerializer(typeof(Templates));
            var file = File.Create("Templates.dat");
            serializator.Serialize(file, templates);
            file.Close();
        }

        public Templates Load()
        {
            if (File.Exists("Templates.dat"))
            {
                Debug.Log("LoadSaveTemplates: Load, file exists");
                var serializator = new XmlSerializer(typeof(Templates));
                var file = File.Open("Templates.dat", FileMode.Open);
                var result = (Templates)serializator.Deserialize(file);
                file.Close();
                return result;
            }
            return null;
        }


        System.Type IService.ServiceType
        {
            get { return typeof (LoadSaveTemplates); }
        }
    }


}
