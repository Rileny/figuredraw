﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace FigureDraw.Logic
{
    /// <summary>
    /// Class for showing figures
    /// </summary>
    [RequireComponent(typeof(LineRenderer))]
    public class Figure : MonoBehaviour
    {
        private LineRenderer lineRenderer = null;

        private void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }
        
        /// <summary>
        /// Draw figure on target points
        /// </summary>
        public void DrawFigure(IEnumerable<Vector2> list)
        {
            var pointsList = list.ToList();
            lineRenderer.SetVertexCount(pointsList.Count);
            for (int i = 0; i < pointsList.Count; i++)
            {
                lineRenderer.SetPosition(i, pointsList[i]);
            }
        }

        /// <summary>
        /// Clear figure
        /// </summary>
        public void Clear()
        {
            lineRenderer.SetVertexCount(0);
        }
    }


}
