﻿//  NOTE. I, Aleksandr Zhankun don't write this code.
//  I remake it from .Net to Unity3d. (Replace System.Drawing.Point on Vector2 and other small fixes) 
//  2016

//
//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE.
//
//  License: GNU General Public License version 3 (GPLv3)
//
//  Email: pavel_torgashov@mail.ru.
//
//  Copyright (C) Pavel Torgashov, 2011. 


using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Math=UnityEngine.Mathf;
namespace FigureDraw.Logic.ContourAnalysis
{
    /// <summary>
    /// Contour
    /// </summary>
    /// <remarks>Vector of complex numbers</remarks>
    [Serializable]
    public class Contour
    {
        public Complex[] array;
        public Rect SourceBoundingRect;
        public  List<Vector2> RealPoints = new List<Vector2>(); 
        public Contour(int capacity)
        {
            array = new Complex[capacity];
        }

        public Contour()
        {
        }

        public int Count
        {
            get
            {
                {
                    if (array == null)
                    {
                        Debug.Log("WTF? Null");
                        return -1;;
                    }
                    else
                    {
                        return array.Length;
                    }
                }
            }
        }

        public Complex this[int i]
        {
            get { return array[i]; }
            set { array[i] = value; }
        }
        
        public Contour(IList<Vector2> points, int startIndex, int count)
            : this(count)
        {
            RealPoints = points.ToList();
            int minX = (int)points[startIndex].x;
            int minY = (int)points[startIndex].y;
            int maxX = minX;
            int maxY = minY;
            int endIndex = startIndex + count;

            for (int i = startIndex; i < endIndex; i++)
            {
                var p1 = points[i];
                var p2 = i == endIndex - 1 ? points[startIndex] : points[i + 1];
                array[i] = new Complex(p2.x - p1.x, -p2.y + p1.y);

                if (p1.x > maxX) maxX = (int)p1.x;
                if (p1.x < minX) minX = (int)p1.x;
                if (p1.y > maxY) maxY = (int)p1.y;
                if (p1.y < minY) minY = (int)p1.y;
            }

            SourceBoundingRect = new Rect(minX, minY, maxX - minX + 1, maxY - minY + 1);
        }



        public Contour(IList<Vector2> points)
            : this(points, 0, points.Count)
        {
            RealPoints = points.ToList();

        }

        public Contour Clone()
        {
            Contour result = new Contour();
            result.array = (Complex[])array.Clone();
            return result;
        }

        /// <summary>
        /// Returns R^2 of difference of norms
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public float DiffR2(Contour c)
        {
            float max1 = 0;
            float max2 = 0;
            float sum = 0;
            for (int i = 0; i < Count; i++)
            {
                float v1 = array[i].Norma;
                float v2 = c.array[i].Norma;
                if (v1 > max1) max1 = v1;
                if (v2 > max2) max2 = v2;
                float v = v1 - v2;
                sum += v * v;
            }
            float max = Math.Max(max1, max2);
            return 1 - sum / Count / max / max;
        }

        public float Norma
        {
            get
            {
                float result = 0;
                foreach (var c in array)
                    result += c.NormaSquare;
                return Math.Sqrt(result);
            }
        }

        /// <summary>
        /// Scalar product
        /// </summary>
        public unsafe Complex Dot(Contour c, int shift)
        {
            var count = Count;
            float sumA = 0;
            float sumB = 0;
            fixed (Complex* ptr1 = &array[0])
            fixed (Complex* ptr2 = &c.array[shift])
            fixed (Complex* ptr22 = &c.array[0])
            fixed (Complex* ptr3 = &c.array[c.Count - 1])
            {
                Complex* p1 = ptr1;
                Complex* p2 = ptr2;
                for (int i = 0; i < count; i++)
                {
                    Complex x1 = *p1;
                    Complex x2 = *p2;
                    sumA += x1.a * x2.a + x1.b * x2.b;
                    sumB += x1.b * x2.a - x1.a * x2.b;

                    p1++;
                    if (p2 == ptr3)
                        p2 = ptr22;
                    else
                        p2++;
                }
            }
            return new Complex(sumA, sumB);
        }

        /// <summary>
        /// Intercorrelcation function (ICF)
        /// </summary>
        public Contour InterCorrelation(Contour c)
        {
            int count = Count;
            Contour result = new Contour(count);
            for (int i = 0; i < count; i++)
                result.array[i] = Dot(c, i);

            return result;
        }

        /// <summary>
        /// Intercorrelcation function (ICF)
        /// maxShift - max deviation (left+right)
        /// </summary>
        public Contour InterCorrelation(Contour c, int maxShift)
        {
            Contour result = new Contour(maxShift);
            int i = 0;
            int count = Count;
            while (i < maxShift / 2)
            {

                result.array[i] = Dot(c, i);
                result.array[maxShift - i - 1] = Dot(c, c.Count - i - 1);
                i++;
            }
            return result;
        }


        /// <summary>
        /// Autocorrelation function (ACF)
        /// </summary>
        public unsafe Contour AutoCorrelation(bool normalize)
        {
            int count = Count / 2;
            Contour result = new Contour(count);
            fixed (Complex* ptr = &result.array[0])
            {
                Complex* p = ptr;
                float maxNormaSq = 0;
                for (int i = 0; i < count; i++)
                {
                    *p = Dot(this, i);
                    float normaSq = (*p).NormaSquare;
                    if (normaSq > maxNormaSq)
                        maxNormaSq = normaSq;
                    p++;
                }
                if (normalize)
                {
                    maxNormaSq = Math.Sqrt(maxNormaSq);
                    p = ptr;
                    for (int i = 0; i < count; i++)
                    {
                        *p = new Complex((*p).a / maxNormaSq, (*p).b / maxNormaSq);
                        p++;
                    }
                }
            }

            return result;
        }


        public void Normalize()
        {
            //find max norma
            float max = FindMaxNorma().Norma;
            //normalize
            if (max > float.Epsilon)
                Scale(1 / max);
        }

        /// <summary>
        /// Finds max norma item
        /// </summary>
        public Complex FindMaxNorma()
        {
            float max = 0f;
            Complex res = default(Complex);
            foreach (var c in array)
                if (c.Norma > max)
                {
                    max = c.Norma;
                    res = c;
                }
            return res;
        }


        /// <summary>
        /// Scalar product
        /// </summary>
        public Complex Dot(Contour c)
        {
            return Dot(c, 0);
        }

        public void Scale(float scale)
        {
            for (int i = 0; i < Count; i++)
                this[i] = scale * this[i];
        }

        public void Mult(Complex c)
        {
            for (int i = 0; i < Count; i++)
                this[i] = c * this[i];
        }

        public void Rotate(float angle)
        {
            var cosA = Math.Cos(angle);
            var sinA = Math.Sin(angle);
            for (int i = 0; i < Count; i++)
                this[i] = this[i].Rotate(cosA, sinA);
        }

        /// <summary>
        /// Normalized Scalar Product
        /// </summary>
        public Complex NormDot(Contour c)
        {
            var count = this.Count;
            float sumA = 0;
            float sumB = 0;
            float norm1 = 0;
            float norm2 = 0;
            for (int i = 0; i < count; i++)
            {
                var x1 = this[i];
                var x2 = c[i];
                sumA += x1.a * x2.a + x1.b * x2.b;
                sumB += x1.b * x2.a - x1.a * x2.b;
                norm1 += x1.NormaSquare;
                norm2 += x2.NormaSquare;
            }

            float k = (float)1d / Math.Sqrt(norm1 * norm2);
            return new Complex(sumA * k, sumB * k);
        }

        /// <summary>
        /// Discrete Fourier Transform
        /// </summary>
        public Contour Fourier()
        {
            int count = Count;
            Contour result = new Contour(count);
            for (int m = 0; m < count; m++)
            {
                Complex sum = new Complex(0, 0);
                float k = (float)-2d * Math.PI * m / count;
                for (int n = 0; n < count; n++)
                    sum += this[n].Rotate(k * n);

                result.array[m] = sum;
            }

            return result;
        }


        public float Distance(Contour c)
        {
            float n1 = this.Norma;
            float n2 = c.Norma;
            return n1 * n1 + n2 * n2 - 2 * (this.Dot(c).a);
        }

        /// <summary>
        /// Changes length of contour (equalization)
        /// </summary>
        public void Equalization(int newCount)
        {
            if (newCount > Count)
                EqualizationUp(newCount);
            else
                EqualizationDown(newCount);
        }

        private void EqualizationUp(int newCount)
        {
            Complex currPoint = this[0];
            Complex[] newPoint = new Complex[newCount];

            for (int i = 0; i < newCount; i++)
            {
                float index = (float)1d * i * Count / newCount;
                int j = (int)index;
                float k = index - j;
                if (j == Count - 1)
                    newPoint[i] = this[j];
                else
                    newPoint[i] = this[j] * (1 - k) + this[j + 1] * k;
            }

            array = newPoint;
        }

        private void EqualizationDown(int newCount)
        {
            Complex currPoint = this[0];
            Complex[] newPoint = new Complex[newCount];

            for (int i = 0; i < Count; i++)
                newPoint[i * newCount / Count] += this[i];

            array = newPoint;
        }

        public Vector2[] GetPoints(Vector2 startPoint)
        {
            Vector2[] result = new Vector2[Count + 1];
            Vector2 sum = startPoint;
            result[0] = Round(sum);
            for (int i = 0; i < Count; i++)
            {
                //sum = sum.Offset((float)array[i].a, -(float)array[i].b);
                sum = Offset(sum, array[i]);
                result[i + 1] = Round(sum);
            }

            return result;
        }
       
        // <summary>
        // Translate point by special Point 
        // </summary>
        private Vector2 Offset(Vector2 point, Complex dislocatePoint)
        {
            return new Vector2(point.x + dislocatePoint.a, point.y - dislocatePoint.b);
        }
        /// <summary>
        /// Rounding point coordinates to nearest integer
        /// </summary>
        private Vector2 Round(Vector2 pointF)
        {
            return new Vector2((int)pointF.x, (int)pointF.y);
        }

        public Rect GetBoundsRect()
        {
            float minX = 0, maxX = 0, minY = 0, maxY = 0;
            float sumX = 0, sumY = 0;
            for (int i = 0; i < Count; i++)
            {
                var v = array[i];
                sumX += v.a;
                sumY += v.b;
                if (sumX > maxX) maxX = sumX;
                if (sumX < minX) minX = sumX;
                if (sumY > maxY) maxY = sumY;
                if (sumY < minY) minY = sumY;
            }

            return new Rect((float)minX, (float)minY, (float)(maxX - minX), (float)(maxY - minY));
        }
    }

}
