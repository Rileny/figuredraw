﻿//  NOTE. I, Aleksandr Zhankun don't write this code.
//  I remake it from .Net to Unity3d. (Replace System.Drawing.Point on Vector2 and other small fixes) 
//  2016

//
//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE.
//
//  License: GNU General Public License version 3 (GPLv3)
//
//  Email: pavel_torgashov@mail.ru.
//
//  Copyright (C) Pavel Torgashov, 2011. 
using Math=UnityEngine.Mathf;

namespace FigureDraw.Logic.ContourAnalysis
{
    public class FoundTemplateDesc
    {
        public float rate;
        public Template template;
        public Template sample;
        public float angle;

        public float scale
        {
            get
            {
                return Math.Sqrt(sample.sourceArea / template.sourceArea);
            }
        }
    }

}
