﻿//  NOTE. I, Aleksandr Zhankun don't write this code.
//  I remake it from .Net to Unity3d. (Replace System.Drawing.Point on Vector2 and other small fixes) 
//  2016

//
//  THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY
//  KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
//  IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR
//  PURPOSE.
//
//  License: GNU General Public License version 3 (GPLv3)
//
//  Email: pavel_torgashov@mail.ru.
//
//  Copyright (C) Pavel Torgashov, 2011. 


using System;
using Math=UnityEngine.Mathf;
namespace FigureDraw.Logic.ContourAnalysis
{
    /// <summary>
    /// Complex number
    /// </summary>
    [Serializable]
    public struct Complex
    {
        public float a;
        public float b;

        public Complex(float a, float b)
        {
            this.a = a;
            this.b = b;
        }

        public static Complex FromExp(float r, float angle)
        {
            return new Complex(r * Math.Cos(angle), r * Math.Sin(angle));
        }

        public float Angle
        {
            get
            {
                return Math.Atan2(b, a);
            }
        }

        public override string ToString()
        {
            return a + "+i" + b;
        }

        public float Norma
        {
            get { return Math.Sqrt(a * a + b * b); }
        }

        public float NormaSquare
        {
            get { return a * a + b * b; }
        }

        public static Complex operator +(Complex x1, Complex x2)
        {
            return new Complex(x1.a + x2.a, x1.b + x2.b);
        }

        public static Complex operator *(float k, Complex x)
        {
            return new Complex(k * x.a, k * x.b);
        }

        public static Complex operator *(Complex x, float k)
        {
            return new Complex(k * x.a, k * x.b);
        }

        public static Complex operator *(Complex x1, Complex x2)
        {
            return new Complex(x1.a * x2.a - x1.b * x2.b, x1.b * x2.a + x1.a * x2.b);
        }

        public float CosAngle()
        {
            return a / Math.Sqrt(a * a + b * b);
        }

        public Complex Rotate(float CosAngle, float SinAngle)
        {
            return new Complex(CosAngle * a - SinAngle * b, SinAngle * a + CosAngle * b);
        }

        public Complex Rotate(float Angle)
        {
            var CosAngle = Math.Cos(Angle);
            var SinAngle = Math.Sin(Angle);
            return new Complex(CosAngle * a - SinAngle * b, SinAngle * a + CosAngle * b);
        }
    }


}
