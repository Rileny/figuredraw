﻿using System;
using UnityEngine;
using System.Collections.Generic;
using FigureDraw.Logic.ContourAnalysis;
using UnityEngine.EventSystems;
using UnityLibrary.Extensions;

namespace FigureDraw.Logic
{
    public class DrawController : MonoBehaviour, IBeginDragHandler, IDragHandler,IEndDragHandler
    {
        #region Events
        /// <summary>
        /// Send figure/contour after EndDrag
        /// </summary>
        public event Action<Template> SendResult = delegate { };
        #endregion

        #region Fields
        /// <summary>
        /// Points count for create corelation function
        /// </summary>
        [SerializeField] private int contourSize = 30;
        
        /// <summary>
        /// Visual effects when drawingS
        /// </summary>
        [SerializeField] private ParticleSystem effect = null;
        
        private List<Vector2> firurePoints=new List<Vector2>();
        private LineRenderer lineRenderer = null;
        private int count = 0;
        
        /// <summary>
        /// Block or Unblock drawing feature
        /// </summary>
        private bool block = false;

      
        #endregion

        #region Unity Events
        private void Awake()
        {
            this.Check("effect", effect != null, LogType.Error);
            lineRenderer = GetComponent<LineRenderer>();
            if(lineRenderer!=null)
                lineRenderer.SetVertexCount(0);
            block = false;
        }

        #endregion
        
        #region Drag Interfaces implement
        void IBeginDragHandler.OnBeginDrag(PointerEventData eventData)
        {
            if (!block)
                effect.Play();
            Draw(eventData.position);
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            Draw(eventData.position);
        }

        void IEndDragHandler.OnEndDrag(PointerEventData eventData)
        {
            effect.Stop();
            Draw(eventData.position);
            if (!block)
            {
                SendResult(new Template(firurePoints.ToArray(), 5, contourSize));
                block = true;
            }

        }
        #endregion
        
        #region Methods
        
        /// <summary>
        /// Draw lines with Linerenderer
        /// </summary>
        /// <param name="position"></param>
        private void Draw(Vector3 position)
        {
            if (block) return;
            if (lineRenderer == null) return;
            count++;
            var worldPositon=Camera.main.ScreenToWorldPoint(position);
            worldPositon.Set(worldPositon.x, worldPositon.y, 0f);
            effect.transform.position = worldPositon;
            lineRenderer.SetVertexCount(count);
            lineRenderer.SetPosition(count-1, worldPositon);
            firurePoints.Add(new Vector2(worldPositon.x, worldPositon.y));           
        }

        /// <summary>
        /// Clear all drawn
        /// </summary>
        public void Clear()
        {
            lineRenderer.SetVertexCount(0);
            firurePoints.Clear();
            count = 0;
        }

        /// <summary>
        /// Unlock drawing feature
        /// </summary>
        public void Unlock()
        {
            block = false;
        }
        #endregion
    }



}
