﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using EJaw.UnityTimers;
using FigureDraw.Logic.ContourAnalysis;
using FigureDraw.View;
using UnityLibrary.Extensions;
using UnityLibrary.Services;
using UnityLibrary.View;

namespace FigureDraw.Logic
{
    /// <summary>
    /// Class controlling basic game feature(timer, changing game mode)
    /// </summary>
    public class GameController : MonoBehaviour
    {
        #region Events
        /// <summary>
        /// Send when need show some GUI Bar
        /// </summary>
        public event Action<bool> ShowGUIBar = delegate { };
        
        /// <summary>
        /// Send when need hide GUI Bar
        /// </summary>
        public event Action<bool> HideGUIBar = delegate { };
        
        /// <summary>
        /// Send when time is up 
        /// </summary>
        public event Action TimeIsUp = delegate { };
        
        /// <summary>
        /// Send when need update time on timer-bar
        /// </summary>
        public event Action<int> UpdateTime = delegate { };
        
        /// <summary>
        /// Send when need update score on score-bar
        /// </summary>
        public event Action<int> ScoreUpdate = delegate { };
        #endregion

        #region Fields
        /// <summary>
        /// Object that responsibility for drawing
        /// </summary>
        [SerializeField] private DrawController drawController = null;
        
        /// <summary>
        /// Object that responsibility for conparison drawn contour with contour templates
        /// </summary>
        [SerializeField] private TemplateFinder templateFinder = null;
        
        /// <summary>
        /// Shows required figure with it drawn points 
        /// </summary>
        [SerializeField] private Figure figure = null;
       
        /// <summary>
        /// Shows required figure with it contour points after corelation 
        /// </summary>
        [SerializeField] private Figure figureContour = null;

        /// <summary>
        /// Time that need for drawing first figure(in seconds)
        /// </summary>
        [SerializeField] private int endTime = 60;
        
        /// <summary>
        /// Decrease end time for next figure on this value after every correct answer/paint (in seconds)
        /// </summary>
        [SerializeField] private int minusTime = 5;
        
        /// <summary>
        /// Timer for checking end time
        /// </summary>
        private Timer timer = new Timer();
        
        /// <summary>
        /// Save score
        /// </summary>
        private int score = 0;

        /// <summary>
        /// For correct decrease time
        /// </summary>
        private int seconds = 0;

        /// <summary>
        /// For decrease time on deltaTime
        /// </summary>
        private int count = 0;
        
        /// <summary>
        /// Responsible for control game mode(game or editor)
        /// </summary>
        private bool editorMode = false;

        /// <summary>
        /// Save templates from table
        /// </summary>
        private List<Template> templateList=new List<Template>();
        
        /// <summary>
        /// Save template in editor mode and save for comparison in game mode
        /// </summary>
        private Template newTemplate = null;
        
        #endregion
        
        #region Unity Events
        private void Awake()
        {
            this.Check("drawController", drawController != null, LogType.Error);
            this.Check("templateFinder", templateFinder != null, LogType.Error);
            this.Check("figure", figure != null, LogType.Error);
            
            Container.AddOrReplace(new LoadSaveTemplates());
        }

       private void OnEnable()
        {
            if (drawController != null)
                drawController.SendResult += OnSendResult;
        }

        private void OnDisable()
        {
            if (drawController != null)
                drawController.SendResult -= OnSendResult;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Called from GUI
        /// </summary>
        public void Begin()
        {
            StopTimer();
            drawController.Clear();
            drawController.Unlock();
            seconds = 0;
            score = 0;
            templateList.Clear();
            var templatesBaseCopy = Container.Get<LoadSaveTemplates>().Load();
            if (templatesBaseCopy != null)
            {
                foreach (var tp in templatesBaseCopy.templatesList)
                {
                    templateList.Add(tp);
                }
            }
          

            ShowGUIBar(editorMode);
            if (!editorMode)
            {
                StartTimer();
                ShowFigure(templateList.FirstOrDefault());
            }         
        }

        /// <summary>
        /// Clear all setings when game is over or exit from some game modes
        /// </summary>
        public void Close()
        {
            figure.Clear();
            figureContour.Clear();
            HideGUIBar(editorMode);
            StopTimer();
            drawController.Clear();
            templateList.Clear();
            seconds = 0;
            count = 0;
        }


        private void StartTimer()
        {
            seconds = endTime-minusTime*count;
            timer.StartInfiniteTimer(1f);
            timer.Elapsed += OnTimerElapsed;
        }

        private void StopTimer()
        {
            timer.Stop();
            timer.Elapsed -= OnTimerElapsed;
        }

        /// <summary>
        /// Checking and update time every seconds
        /// </summary>
        private void OnTimerElapsed(float time)
        {
            seconds--;
            UpdateTime(seconds);
            if (seconds < 0)
            {
                GameOver();
                StopTimer();
            }
        }

        /// <summary>
        /// Processing result in different game modes
        /// </summary>
        private void OnSendResult(Template template)
        {
            if (!editorMode)
            {
                var templates = new List<Template>();
                templates.Add(newTemplate);
                var result = templateFinder.FindTemplate(templates, template);
                if (result == null)
                {
                    OnFailed();
                }
                else
                {
                    OnCorrect();
                }
            }
            else
            {
                newTemplate = template;
            }
        }
      
        /// <summary>
        /// Add template to table in editor mode
        /// </summary>
        public void AddTemplate(int id, string name)
        {
            if (templateList.Count <= id)
            {
                templateList.Add(newTemplate);
            }
            else
            {
                templateList[id] = newTemplate;
            }

            Debug.Log("GameController: New template added - id=" + id + "  name=" + name);
            drawController.Clear();
            drawController.Unlock();
        }

        /// <summary>
        /// Clear your picture and unlock drawing feature in editor mode
        /// </summary>
        public void ClearDesk()
        {
            drawController.Clear();
            drawController.Unlock();
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void ShowNextFigure()
        {
            ClearDesk();
            ShowFigure(templateList.Random());
        }

        /// <summary>
        /// Drawing some figure on field
        /// </summary>
        /// <param name="target"></param>
        private void ShowFigure(Template target)
        {
            if (target == null)
            {
                Displayer.Instance.Display(
                    new MessageWindow.MessageWindowData("Sorry, figures ended!\n Let's draw to draw again first",
                        () =>
                        {
                            Close();
                            TimeIsUp();
                        } 
                ));
                return;
            }
            count++;
            newTemplate = target;
            figure.DrawFigure(target.contour.RealPoints);
            //TODO some times library get NullException when draw figure in corelation points, fix later
            //figureContour.DrawFigure(target.contour.GetPoints(target.startPoint));
            templateList.Remove(target);
        }

        /// <summary>
        /// Update score and show win window if correct
        /// </summary>
        private void OnCorrect()
        {
            score++;
            ScoreUpdate(score);
            StopTimer();
            Displayer.Instance.Display(new MessageWindow.MessageWindowData("Bravo, it's correct!\n Let's draw next figure",
                () =>
                {
                    StartTimer();
                    ShowFigure(templateList.Random());
                    drawController.Unlock();
                    drawController.Clear();
                }));
            Debug.Log("GameController: Bravo, it's correct! Let's draw next figure");
        }

        /// <summary>
        /// Clear desk if figure is uncorrect
        /// </summary>
        private void OnFailed()
        {
            Debug.Log("GameController: Sorry, it's uncorrect, please, try again");
            this.DelayedCall(0.2f, () =>
            {
               ClearDesk();
            });
        }

        /// <summary>
        /// Show TimeIsUp window
        /// </summary>
        private void GameOver()
        {
            Debug.Log("GameController: Sorry, time is up. You lose!");
            Displayer.Instance.Display(
                new MessageWindow.MessageWindowData("Sorry, time is up!\n Your score: " + score, () =>
                {
                    Close();
                    TimeIsUp();
                }));
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void SetEditorMode(bool mode)
        {
            editorMode = !editorMode;
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void Save()
        {
            Container.Get<LoadSaveTemplates>().Save(new Templates(templateList));    
        }

        /// <summary>
        /// Called from GUI 
        /// </summary>
        public void ExitApplication()
        {
            Application.Quit();
        }
        #endregion
    }

}
