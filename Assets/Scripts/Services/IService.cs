﻿using System;

namespace UnityLibrary.Services
{
    /// <summary>
    /// Implement this interface if you want mark object as Service
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Define service type. Its necessary for interfaces if you
        /// want change services on one interface.
        /// </summary>
        Type ServiceType { get; }
    }
}
