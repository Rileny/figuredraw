﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UnityLibrary.Services
{
    /// <summary>
    /// Has all static game services that can be present only in one instance.
    /// For example: shops, profiles etc.
    /// This is attempt to create DeppendecyInjection =)
    /// </summary>
    public static class Container
    {
        /// <summary>
        /// All services in the game
        /// </summary>
        private static readonly Dictionary<Type, IService> services = new Dictionary<Type, IService>();

        /// <summary>
        /// Add new service if service isn't already exist to the container
        /// </summary>
        public static void Add<T>(T service) where T : IService
        {
            var t = service.ServiceType;
            if (services.ContainsKey(t))
            {
                Debug.LogError(
                    string.Format(
                        "Container: failed add new service (type={0}, obj={1}) to the container due to services (type={0}, obj={2}) already exist. Use AddOrReplace method if you want change old service.",
                        t, service, services[t]));
            }
            else
            {
                services.Add(t, service);
                Debug.Log(string.Format(
                    "Container: object (type={0}, obj={1}) succeffully added to the container.",
                    t, service));
            }
        }

        /// <summary>
        /// Add new service to the container or replace old if service already exist.
        /// </summary>
        public static void AddOrReplace<T>(T service) where T : IService
        {
            var t = service.ServiceType;
            if (services.ContainsKey(t))
            {
                var old = services[t];
                services.Remove(t);
                Debug.Log(
                    string.Format("Container: service (type={0}, obj={1}) replaced by service (type={0}, obj={2}).",
                        t, old, service));
            }
            else
            {
                Debug.Log(string.Format(
                    "Container: service (type={0}, obj={1}) succeffully added to the container.", t, service));
            }

            services.Add(t, service);
        }

        /// <summary>
        /// Get service from container.
        /// </summary>
        public static T Get<T>() where T : class, IService
        {
            var t = typeof (T);
            if (services.ContainsKey(t))
                return services[t] as T;

            Debug.LogError(string.Format("Container: service {0} isn't exist in the container.", t));
            return default(T);
        }

        /// <summary>
        /// Get service from container. You can set default service
        /// if needed service isn't exist in the container.
        /// </summary>
        public static T Get<T>(T defaultService) where T : class, IService
        {
            var t = typeof (T);
            if (services.ContainsKey(t))
                return services[t] as T;

            Debug.LogWarning(string.Format("Container: service {0} isn't exist in the container.", t));
            return defaultService;
        }

        /// <summary>
        /// Return true if container has current service
        /// </summary>
        public static bool Contains(Type type)
        {
            return services.ContainsKey(type);
        }

        /// <summary>
        /// Remove service from the container if it exist there.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public static void Remove<T>() where T : IService
        {
            var t = typeof (T);
            if (services.ContainsKey(t))
            {
                services.Remove(t);
                Debug.Log(string.Format("Container: service {0} removed.", t));
            }
        }

        /// <summary>
        /// Remove all services from conteiner
        /// </summary>
        public static void Clear()
        {
            services.Clear();
            Debug.Log("Container: all services removed.");
        }
    }
}
