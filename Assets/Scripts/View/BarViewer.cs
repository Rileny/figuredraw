﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityLibrary.Extensions;
using UnityLibrary.View;

namespace FigureDraw.View
{
    public class BarViewer : BaseViewer
    {
        /// <summary>
        /// Text that show time
        /// </summary>
        [SerializeField] private Text timeText = null;
        
        /// <summary>
        /// Text that show score
        /// </summary>
        [SerializeField] private Text scoreText = null;
        
        protected override void Awake()
        {
            base.Awake();
            this.Check("timeText", timeText, LogType.Error);
            this.Check("scoreText", scoreText, LogType.Error);
        }

        
        public void ChangeTime(int seconds)
        {
            timeText.text = String.Format("Time: {0:00}:{1:00}", seconds / 60, seconds % 60f);
        }

        public void ChangeScore(int score)
        {
            scoreText.text = "Score:  " + score;
        }
      
    }


}
