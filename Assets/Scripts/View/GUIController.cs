﻿using UnityEngine;
using System.Collections;
using FigureDraw.Logic;
using UnityLibrary.Extensions;

namespace FigureDraw.View
{
    public class GUIController : MonoBehaviour
    {
        #region Fields
        /// <summary>
        /// For subscribing on events
        /// </summary>
        [SerializeField] private GameController gameController = null;

        /// <summary>
        /// Bar in editor mode
        /// </summary>
        [SerializeField] private EditorBarViewer editorBarViewer = null;
        
        /// <summary>
        /// Bar in regular mode
        /// </summary>
        [SerializeField] private BarViewer barViewer = null;
        
        /// <summary>
        /// Main menu with button Start
        /// </summary>
        [SerializeField] private StartMenu startMenu = null;
        #endregion

        #region Uniry Events
        private void Awake()
        {
            this.Check("gameController", gameController != null, LogType.Error);
            this.Check("editorBarViewer", editorBarViewer != null, LogType.Error);
            this.Check("barViewer", barViewer != null, LogType.Error);
            this.Check("startMenu", startMenu != null, LogType.Error);
        }

        private void OnEnable()
        {
            gameController.ShowGUIBar += OnShowGUIBar;
            gameController.HideGUIBar += OnHideGUIbar;
            gameController.TimeIsUp += OnTimeIsUp;
            gameController.UpdateTime += OnTimeUpdate;
            gameController.ScoreUpdate += OnScoreUpdate;
            
        }

        private void OnDisable()
        {
            gameController.ShowGUIBar -= OnShowGUIBar;
            gameController.HideGUIBar -= OnHideGUIbar;
            gameController.TimeIsUp -= OnTimeIsUp;
            gameController.UpdateTime -= OnTimeUpdate;
            gameController.ScoreUpdate -= OnScoreUpdate;

        }
        #endregion

        #region Methods
        private void OnShowGUIBar(bool editorMode)
        {
            if (editorMode)
            {
                editorBarViewer.Show();
            }
            else
            {
                barViewer.Show();
            }
        }

        private void OnHideGUIbar(bool editorMode)
        {
            if (editorMode)
            {
                editorBarViewer.Hide();
            }
            else
            {
                barViewer.Hide();
            }
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void OnAddButtonDown()
        {
            gameController.AddTemplate(editorBarViewer.Id, editorBarViewer.NameTemplate);
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void OnClearButtonDown()
        {
            gameController.ClearDesk();
        }

        /// <summary>
        /// Called from GUI
        /// </summary>
        public void OnBackButtonDown()
        {
            gameController.Close();
            startMenu.Show();
        }

        private void OnTimeIsUp()
        {
            startMenu.Show();
        }

        private void OnTimeUpdate(int seconds)
        {
            barViewer.ChangeTime(seconds);
        }

        private void OnScoreUpdate(int score)
        {
            barViewer.ChangeScore(score);
        }
        #endregion
    }


}
