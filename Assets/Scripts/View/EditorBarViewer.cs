﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityLibrary.Extensions;
using UnityLibrary.View;

namespace FigureDraw.View
{
    /// <summary>
    /// Bar in editor mode
    /// </summary>
    public class EditorBarViewer : BaseViewer
    {
        #region Events
        /// <summary>
        /// Send id-record in table that need update
        /// </summary>
        public event Action<int> SendId = delegate { };

        /// <summary>
        /// Send name-template 
        /// </summary>
        public event Action<string> SendName = delegate { };
        #endregion

        #region Fields
        /// <summary>
        /// InputField for get Id
        /// </summary>
        [SerializeField] private InputField idInputField = null;
        
        /// <summary>
        /// InputField for get name-template
        /// </summary>
        [SerializeField] private InputField nameInputField = null;

        /// <summary>
        /// Id-record in table
        /// </summary>
        private int id = 0;
        
        /// <summary>
        /// Name of template
        /// </summary>
        private string nameTemplate = "";
        #endregion
        
        #region Properties
        /// <summary>
        /// Id-record in table
        /// </summary>
        public int Id
        {
            get { return id; }
        }

        /// <summary>
        /// Name of template
        /// </summary>
        public string NameTemplate
        {
            get { return nameTemplate; }
        }
        #endregion

        #region Unity Events
        protected override void Awake()
        {
            base.Awake();
            this.Check("idInputField", idInputField != null, LogType.Error);
            this.Check("nameInputField", nameInputField != null, LogType.Error);
        }
        #endregion

        #region Methods
        public void OnIdEndEdit(string input)
        {
            var text = idInputField.text;
            if (text == String.Empty)
            {
                idInputField.text = "";
                return;
            }         
            if (!int.TryParse(text, out id))
            {
                idInputField.text = "";
                return;
            }
            id = Mathf.Clamp(id, 0, 4);
        }

        public void OnNameEndEdit(string input)
        {
            var text = nameInputField.text;
            if (text == String.Empty) return;
            nameTemplate = text;
        }

        #endregion
    }


}
