﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityLibrary.Additions;
using UnityLibrary.Extensions;
using UnityLibrary.View;

namespace FigureDraw.View
{
    /// <summary>
    /// Window that show some messages
    /// </summary>
    public sealed class MessageWindow : BaseTargetViewer<MessageWindow.MessageWindowData>
    {
        public sealed class MessageWindowData
        {
            /// <summary>
            /// Text message
            /// </summary>
            public readonly string message = string.Empty;
            public readonly string okButtonText = "Ok";
            
            /// <summary>
            /// Event that send when button "Ok" pressed
            /// </summary>
            public readonly Action onClose = delegate { };

            public MessageWindowData(string message, string okButtonText = "Ok")
            {
                this.message = message;
                this.okButtonText = okButtonText;
            }

            public MessageWindowData(string message, Action onClose, string okButtonText = "Ok")
                : this(message, okButtonText)
            {
                this.onClose = onClose;
            }
          
        }

        [SerializeField] private Text messageText = null;
        [SerializeField] private Text okText = null;
        [SerializeField] private Text titleText = null;

        protected override void Awake()
        {
            base.Awake();
            this.Check("messageText", messageText != null, LogType.Error);
            this.Check("titleText", titleText != null, LogType.Error);
        }

        /// <summary>
        /// Show window
        /// </summary>
        public override void Show(MessageWindowData target)
        {
            base.Show(target);
            GUIHelper.SetText(messageText, Target.message);
            GUIHelper.SetText(okText, Target.okButtonText);
        }

        /// <summary>
        /// Hide window
        /// </summary>
        public override void Hide()
        {
            if (Target != null)
                EventSender.SendEvent(Target.onClose);
            base.Hide();
        }
    }
}
