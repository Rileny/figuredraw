﻿using System;

namespace UnityLibrary.View
{
    /// <summary>
    /// Can have data for show or hide something
    /// </summary>
    public interface ITargetViewer
    {
        /// <summary>
        /// Return target type
        /// </summary>
        Type TargetType { get; }

        /// <summary>
        /// True if viewer need to
        /// be hide on Awake
        /// </summary>
        bool HideOnAwake { get; }

        /// <summary>
        /// Show any target
        /// </summary>
        void Show(object target);

        /// <summary>
        /// Hide viewer
        /// </summary>
        void Hide();
    }
}
