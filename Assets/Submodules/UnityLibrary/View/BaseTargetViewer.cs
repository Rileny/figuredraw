﻿using System;
using UnityLibrary.Additions;

namespace UnityLibrary.View
{
    public abstract class BaseTargetViewer<TTarget> : Viewer, ITargetViewer where TTarget : class
    {
        protected TTarget Target { get; set; }

        #region IGuiTargetViewer

        Type ITargetViewer.TargetType
        {
            get
            {
                return typeof (TTarget);
            }
        }

        bool ITargetViewer.HideOnAwake
        {
            get { return hideOnAwake; }
        }

        void ITargetViewer.Show(object target)
        {
            Show(target as TTarget);
        }

        public virtual void Show(TTarget target)
        {
            Target = target;
            CheckObjectToHide();
            GameObjectHelper.SetActive(objectToHide, true);
        }

        public virtual void Hide()
        {
            Target = null;
            CheckObjectToHide();
            GameObjectHelper.SetActive(objectToHide, false);
        }

        #endregion
    }
}
