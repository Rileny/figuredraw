﻿using UnityLibrary.Additions;

namespace UnityLibrary.View
{
    /// <summary>
    /// Basic realization of viewer wothout target
    /// </summary>
    public abstract class BaseViewer : Viewer, IViewer
    {
        #region IGuiViewer

        bool IViewer.HideOnAwake
        {
            get { return hideOnAwake; }
        }

        public virtual void Show()
        {
            CheckObjectToHide();
            GameObjectHelper.SetActive(objectToHide, true);
        }

        public virtual void Hide()
        {
            CheckObjectToHide();
            GameObjectHelper.SetActive(objectToHide, false);
        }

        #endregion
    }
}
