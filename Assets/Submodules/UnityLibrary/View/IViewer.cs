﻿
namespace UnityLibrary.View
{
    /// <summary>
    /// Simple viewer that 
    /// only show or hide something without target
    /// </summary>
    public interface IViewer
    {
        /// <summary>
        /// True if viewer need to
        /// be hide on Awake
        /// </summary>
        bool HideOnAwake { get; }

        /// <summary>
        /// Show viewer
        /// </summary>
        void Show();

        /// <summary>
        /// Hide viewer
        /// </summary>
        void Hide();
    }
}
