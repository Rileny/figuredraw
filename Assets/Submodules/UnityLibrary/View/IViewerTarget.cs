﻿using System;

namespace UnityLibrary.View
{
    /// <summary>
    /// This interface usefull if we need 
    /// specialize viewer target type
    /// </summary>
    public interface IViewerTarget
    {
        /// <summary>
        /// Type of viewer target
        /// </summary>
        Type TargetType { get; }

        /// <summary>
        /// Real viewer target
        /// </summary>
        object Target { get; }
    }
}
