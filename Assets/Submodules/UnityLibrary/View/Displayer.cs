﻿using System;
using UnityLibrary.Additions;

namespace UnityLibrary.View
{
    /// <summary>
    /// Use this class for display objects. Its events listen GuiController
    /// </summary>
    public sealed class Displayer : Singletone<Displayer>
    {
        public event Action<object, IViewerTarget> DisplayTargetViewer = delegate { };
        public event Action<Type> DisplayViewer = delegate { };
        public event Action<Type> Hided = delegate { };
        public event Action<object> TargetHided = delegate { };

        /// <summary>
        /// Display viewer by its object
        /// </summary>
        /// <param name="target"></param>
        public void Display(object target)
        {
            EventSender.SendEvent(DisplayTargetViewer, target, null);
        }

        /// <summary>
        /// Display viewer by interface
        /// Interface realization have to implement also IViewerTarget
        /// </summary>
        /// <param name="target"></param>
        public void DisplayViewerWithTarget(IViewerTarget target)
        {
            EventSender.SendEvent(DisplayTargetViewer, null, target);
        }

        /// <summary>
        /// Display viewer by its type
        /// </summary>
        /// <param name="viewerType"></param>
        public void Display(Type viewerType)
        {
            EventSender.SendEvent(DisplayViewer, viewerType);
        }

        /// <summary>
        /// Hide viewer by its type
        /// </summary>
        /// <param name="viewerType"></param>
        public void Hide(Type viewerType)
        {
            EventSender.SendEvent(Hided, viewerType);
        }

        /// <summary>
        /// Hide viewer by its object
        /// </summary>
        /// <param name="target"></param>
        public void Hide(object target)
        {
            EventSender.SendEvent(TargetHided, target);
        }

    }
}
