﻿using UnityEngine;

namespace UnityLibrary.View
{
    /// <summary>
    /// Basic realization for all viewers. 
    /// </summary>
    public abstract class Viewer : MonoBehaviour
    {
        #region Fields

        /// <summary>
        /// For parent that we can show and hide
        /// </summary>
        [SerializeField] protected GameObject objectToHide = null;

        /// <summary>
        /// True if viewer need to
        /// be hide on Awake
        /// </summary>
        [SerializeField] protected bool hideOnAwake = true;

        #endregion

        #region Unity events

        protected virtual void Awake()
        {
            CheckObjectToHide();
        }

        protected virtual void OnDestroy()
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// Check if object to hide is null
        /// we set current gameObject for this
        /// </summary>
        protected void CheckObjectToHide()
        {
            if (objectToHide == null)
                objectToHide = gameObject;
        }

        #endregion
    }
}
