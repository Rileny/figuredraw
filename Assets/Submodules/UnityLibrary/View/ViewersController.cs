﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityLibrary.Extensions;

namespace UnityLibrary.View
{
    /// <summary>
    /// This class work with all viewers. It listen displayer and show needed viewer. 
    /// </summary>
    public class ViewersController : MonoBehaviour
    {
        #region Fields

        private readonly Dictionary<Type, ITargetViewer> targetViewers = new Dictionary<Type, ITargetViewer>();
        private readonly Dictionary<Type, IViewer> simlpeViewers = new Dictionary<Type, IViewer>();

        #endregion

        #region Unity methods
        //Awake
        private void Awake()
        {
            foreach (var v in gameObject.GetGenericComponentsInChildren<ITargetViewer>())
            {
                if (!targetViewers.ContainsKey(v.TargetType))
                    targetViewers.Add(v.TargetType, v);
                if (v.HideOnAwake)
                    v.Hide();
            }

            foreach (var v in gameObject.GetGenericComponentsInChildren<IViewer>())
            {
                if (!simlpeViewers.ContainsKey(v.GetType()) && !targetViewers.ContainsKey(v.GetType()))
                    simlpeViewers.Add(v.GetType(), v);
                if (v.HideOnAwake)
                    v.Hide();
            }

            Displayer.Instance.DisplayTargetViewer += OnDisplayTargetViewer;
            Displayer.Instance.Hided += OnSomethingNeedToHide;
            Displayer.Instance.DisplayViewer += OnDisplaySimpleViewer;
            Displayer.Instance.TargetHided += OnTargetHided;
        }

        private void OnDestroy()
        {
            Displayer.Instance.DisplayTargetViewer -= OnDisplayTargetViewer;
            Displayer.Instance.Hided -= OnSomethingNeedToHide;
            Displayer.Instance.DisplayViewer -= OnDisplaySimpleViewer;
            Displayer.Instance.TargetHided -= OnTargetHided;
        }

        #endregion

        #region Methods

        private void OnDisplayTargetViewer(object obj, IViewerTarget viewerTarget)
        {
            ITargetViewer v = null;
            Type type = null;
            object target = obj;
            if (viewerTarget != null)
            {
                type = viewerTarget.TargetType;
                target = viewerTarget.Target;
            }
            else if (obj != null)
                type = obj.GetType();

            if (type != null && targetViewers.TryGetValue(type, out v))
            {
                v.Show(target);
            }
        }

        private void OnDisplaySimpleViewer(Type type)
        {
            IViewer v = null;
            if (simlpeViewers.TryGetValue(type, out v))
                v.Show();
        }

        private void OnSomethingNeedToHide(Type t)
        {
            ITargetViewer v = null;
            if (targetViewers.TryGetValue(t, out v))
            {
                v.Hide();
            }
            else
            {
                IViewer v1 = null;
                if (simlpeViewers.TryGetValue(t, out v1))
                    v1.Hide();
            }
        }

        private void OnTargetHided(object obj)
        {
            ITargetViewer v = null;
            Type type = null;

            if (obj != null)
                type = obj.GetType();

            if (type != null && targetViewers.TryGetValue(type, out v))
            {
                v.Hide();
            }
        }

        #endregion
    }
}
