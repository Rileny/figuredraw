﻿using System;
using UnityEngine;

namespace UnityLibrary.Debugging
{
  [ExecuteInEditMode]
  public sealed class VersionIndicator : MonoBehaviour
  {
    #region FIELDS
    
    [SerializeField] private TextAsset file;
    [SerializeField] private string defaultVersion = "---";
    [SerializeField] private string versionTemplate = "Version {0}";
    [SerializeField] private int fontSize = 13;
    [SerializeField] private Color textColor = Color.white;
    [SerializeField] private Font font = null;
    [SerializeField] private FontStyle fontStyle = FontStyle.Bold;
    [SerializeField] private TextAnchor anchor = TextAnchor.LowerRight;
    [SerializeField] private Vector2 shift = new Vector2(4, 16);
    [SerializeField] private bool shadow = true;
    [SerializeField] private Color shadowColor = new Color(0, 0, 0, 0.65f);
    [SerializeField] private Vector2 shadowDistance = new Vector2(0, 2);
    private string version = string.Empty;
    private const int maxLength = 100;

    #endregion FIELDS
        
    #region UNITY EVENTS   
    
    private void Awake()
    {
      if (!Debug.isDebugBuild)
      {
        enabled = false; //TODO test it
        return;
      }
      
      fontSize = Math.Max(1, fontSize);
      shift.x = Mathf.Max(0, shift.x);
      shift.y = Mathf.Max(0, shift.y);
      
      string versionValue = defaultVersion;
      if (file != null)
      {
        var ver = file.text;
        if (ver.Length > 0)
          versionValue = ver.Substring(0, Math.Min(ver.Length, maxLength));
        else
          Debug.LogWarning(string.Format("VersionIndicator failed to read '{0}' version file due to file is empty", file.name));
      }
      
      version = string.IsNullOrEmpty(versionTemplate) ? versionValue : string.Format(versionTemplate, versionValue);
    }

    private void OnGUI()
    {
      if (!Debug.isDebugBuild)
        return;

      GUI.skin.label.alignment = anchor;
      GUI.skin.label.fontSize = fontSize;
      GUI.skin.label.fontStyle = fontStyle;
      if (font != null)
        GUI.skin.label.font = font;

      if (shadow)
      {
        DrawLabel(version, shadowColor, shadowDistance);
      }
      DrawLabel(version, textColor, Vector2.zero);
    }
    
    #endregion UNITY EVENTS
    
    private void DrawLabel(string text, Color color, Vector2 displacement)
    {
      //#if !UNITY_EDITOR

      var y = shift.y;
      if (anchor == TextAnchor.MiddleCenter || anchor == TextAnchor.MiddleLeft || anchor == TextAnchor.MiddleRight)
        y = Screen.height/2 - GUI.skin.label.lineHeight;
      else if (anchor == TextAnchor.LowerCenter || anchor == TextAnchor.LowerLeft || anchor == TextAnchor.LowerRight)
        y = Screen.height - shift.y - GUI.skin.label.lineHeight*1.5f;
      
      GUI.color = color;
      if (anchor == TextAnchor.MiddleLeft || anchor == TextAnchor.LowerLeft || anchor == TextAnchor.UpperLeft)
        GUILayout.BeginArea(new Rect(shift.x+displacement.x, y+displacement.y, Screen.width, Screen.height));
      else if (anchor == TextAnchor.MiddleRight || anchor == TextAnchor.LowerRight || anchor == TextAnchor.UpperRight)
        GUILayout.BeginArea(new Rect(-Screen.width+displacement.x, y+displacement.y, Screen.width*2 - shift.x, Screen.height));
      else
        GUILayout.BeginArea(new Rect(displacement.x, y+displacement.y, Screen.width, Screen.height));
      
      GUILayout.Label(text);
      GUILayout.EndArea();

      //#endif
    }
  }
}