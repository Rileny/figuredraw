using UnityEngine;

namespace UnityLibrary.Debugging
{
  public sealed class FPSSetter : MonoBehaviour
  {
    [SerializeField] private int targetFrameRate = 60;

    private void Awake()
    {
      Application.targetFrameRate = targetFrameRate;
    }
  }
}