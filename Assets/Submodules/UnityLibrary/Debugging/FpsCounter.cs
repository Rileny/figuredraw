using UnityEngine;


namespace UnityLibrary.Debugging
{
  /// <summary>
  /// Calculates frames per second and displays it.
  /// Debug class.
  /// </summary>
  [ExecuteInEditMode]
  public sealed class FpsCounter : MonoBehaviour
  {
    #region FIELDS

    [SerializeField] private float updateInterval = 0.5f;

    /// <summary>
    /// Last interval end time.
    /// </summary>
    private float lastInterval = 0;

    /// <summary>
    /// Counted frames.
    /// </summary>
    private int frames = 0;

    private int currentFps = 0;

    private const int fontSize = 20;

    #endregion FIELDS

    #region UNITY EVENTS

    private void Start()
    {
      lastInterval = Time.realtimeSinceStartup;
      frames = 0;
    }

    private void OnGUI()
    {
      ShowFps(currentFps);
    }

    private void Update()
    {
      ++frames;
      var timeNow = Time.realtimeSinceStartup;
      if (timeNow > lastInterval + updateInterval)
      {
        currentFps = Mathf.FloorToInt(frames/(timeNow - lastInterval));
        frames = 0;
        lastInterval = timeNow;
      }
    }

    #endregion UNITY EVENTS

    #region FUNCTIONS

    private void ShowFps(int fps)
    {
      if (fps < 30)
      {
        GUI.skin.label.fontSize = Mathf.RoundToInt(fontSize*1.5f);
        GUI.color = Color.yellow;
      }
      else if (fps < 10)
      {
        GUI.skin.label.fontSize = fontSize*2;
        GUI.color = Color.red;
      }
      else
      {
        GUI.skin.label.fontSize = fontSize;
        GUI.color = Color.green;
      }

      GUILayout.BeginArea(new Rect(0, 0, GUI.skin.label.fontSize*2, GUI.skin.label.fontSize*2));
      GUILayout.Label(fps.ToString());
      GUILayout.EndArea();
    }

    #endregion FUNCTIONS
  }
}