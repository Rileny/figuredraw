#if UNITY_EDITOR
using UnityEditor;

namespace UnityLibrary.EditorClasses
{
  /// <summary>
  /// Used to convert target to specified type, not Object.
  /// </summary>
  public abstract class TargetEditor<TTarget> : Editor where TTarget : class
  {
    public TTarget Target
    {
      get
      {
        return target as TTarget;
      }
    }
  }
}

#endif
