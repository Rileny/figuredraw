﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;

namespace UnityLibrary.EditorClasses.Helpers
{
    public static class PopupsHelper
    {
        public static string StringPopup(string label, string current, IEnumerable<string> data,
            UnityEngine.GUILayoutOption options)
        {
            int index = data.ToList().IndexOf(current);
            index = EditorGUILayout.Popup(label, index, data.ToArray(), options);
            if (index >= 0 && index < data.Count())
                return data.ElementAt(index);
            return current;
        }

        public static string StringPopup(string label, string current, IEnumerable<string> data)
        {
            int index = data.ToList().IndexOf(current);
            index = EditorGUILayout.Popup(label, index, data.ToArray());
            if (index >= 0 && index < data.Count())
                return data.ElementAt(index);
            return current;
        }

        public static int NameToIdPopup(string label, int current, IEnumerable<int> ids, IEnumerable<string> names)
        {
            int index = ids.ToList().IndexOf(current);
            index = EditorGUILayout.Popup(label, index, names.ToArray());
            if (index >= 0 && index < ids.Count())
                return ids.ElementAt(index);

            return -1;
        }

        public static int NameToIdPopup(string label, int current, IEnumerable<int> ids, IEnumerable<string> names,
            UnityEngine.GUILayoutOption options)
        {
            int index = ids.ToList().IndexOf(current);
            index = EditorGUILayout.Popup(label, index, names.ToArray(), options);
            if (index >= 0 && index < ids.Count())
                return ids.ElementAt(index);

            return -1;
        }
    }
}

#endif