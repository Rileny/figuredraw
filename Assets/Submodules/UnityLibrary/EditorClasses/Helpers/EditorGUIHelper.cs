﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace UnityLibrary.EditorClasses.Helpers
{
  public static class EditorGUIHelper
  {
    public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled, params GUILayoutOption[] options)
    {
      GUI.backgroundColor = buttonColor;
      GUI.enabled = buttonEnabled;
      bool button = GUILayout.Button(buttonName, options);
      GUI.enabled = true;
      GUI.backgroundColor = Color.white;

      return button;
    }

    /// <summary>
    /// Create GUI button with name,
    /// color and make it enable or
    /// disable
    /// </summary>
    public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled = true)
    {
      GUI.backgroundColor = buttonColor;
      GUI.enabled = buttonEnabled;
      bool button = GUILayout.Button(buttonName);
      GUI.enabled = true;
      GUI.backgroundColor = Color.white;

      return button;
    }

    public static void Check(string variableName, bool condition, MessageType messageType = MessageType.Info)
    {
      if (!condition)
        EditorGUILayout.HelpBox(string.Format("Invalid value for {0}", variableName), messageType);
    }

  }
}

#endif
