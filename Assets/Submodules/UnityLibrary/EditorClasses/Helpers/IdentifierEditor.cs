﻿
#if UNITY_EDITOR
using System.Linq;
using UnityEditor;
using UnityEngine;

using UnityLibrary.Additions;

namespace UnityLibrary.EditorClasses.Helpers
{
  [CustomEditor(typeof(Identifier), false)]
  public class IdentifierEditor : Editor
  {
    public override void OnInspectorGUI()
    {
      Identifier identifier = target as Identifier;
      EditorGUILayout.LabelField("ID = " + identifier.Id);
      
      if (!identifier.IsIdValid)
      {
        EditorGUILayout.HelpBox("ID is invalid (press 'Identify' button!)", MessageType.Error);
        if (EditorGUIHelper.Button("Identify", Color.red))
        {
          if (!IsIdentifierInScene)
          {
            identifier.Identify();
            EditorUtility.SetDirty(identifier);
          }
          else
            EditorUtility.DisplayDialog("Identify error", "Scene identifier may be identified only!", "Ok");
        }
      }
      else if (EditorGUIHelper.Button("Reset identifier", Color.green))
      {
        if (EditorUtility.DisplayDialog("Reset identifier", "Are you sure?", "Yes", "No"))
        {
          if (IsIdentifierInScene)
            identifier.ResetIdentifier();
          else
            identifier.ClearIdentifier();

          EditorUtility.SetDirty(identifier);
        }
      }
    }

    private bool IsIdentifierInScene 
    {
      get
      {
        var identifiers = GameObject.FindObjectsOfType(typeof (Identifier)).Cast<Identifier>();
        return (identifiers.Any(i => i == target));
      }
    }
  }
}
#endif