using UnityEngine;

namespace UnityLibrary.Extensions
{
  public static class NavMeshAgentExtensions
  {
    public static bool SetDestinationCarefully(this NavMeshAgent agent, Vector3 target)
    {
      if (agent != null && agent.enabled)
        return agent.SetDestination(target);
        
      return false;
    }

    public static bool SetDestinationCarefully(this NavMeshAgent agent, Transform target)
    {
      return SetDestinationCarefully(agent, target.position);
    }

    public static void StopAndResetPath(this NavMeshAgent agent)
    {
      agent.Stop();
      agent.ResetPath();
    }
  }
}
