using System;
using System.Collections;
using UnityEngine;
using UnityLibrary.Additions;

namespace UnityLibrary.Extensions
{
    public static class MonoBehaviourExtensions
    {
        /// <summary>
        /// Get component from game object or add it to them
        /// </summary>
        public static T GetOrAddComponent<T>(this MonoBehaviour mono) where T : MonoBehaviour
        {
            if (mono == null)
                return null;
            return mono.GetComponent<T>() ?? mono.gameObject.AddComponent<T>();
        }

        /// <summary>
        /// Use this functions for check
        /// editor variales
        /// </summary>
        public static bool Check(this MonoBehaviour mono, string variableName, bool condition, LogType logType)
        {
            if (!condition)
            {
                string message = string.Format("'{0}' has invalid '{1}' value", mono, variableName);
                Log(message, mono, logType);
            }
            return condition;
        }

        /// <summary>
        /// Use this function for do 
        /// somethig after some time
        /// </summary>
        public static void DelayedCall(this MonoBehaviour mono, float delay, Action action)
        {
            mono.StartCoroutine(DelayedCall(delay, action));
        }

        private static IEnumerator DelayedCall(float delay, Action action)
        {
            yield return new WaitForSeconds(delay);
            EventSender.SendEvent(action);
        }

        private static void Log(string message, MonoBehaviour mono, LogType logType = LogType.Log)
        {
            if (logType == LogType.Error)
                Debug.LogError(message, mono);
            else if (logType == LogType.Warning)
                Debug.LogWarning(message, mono);
            else
                Debug.Log(message, mono);
        }

        [Obsolete("Use ImportantDebug class instead")]
        public static void ImportantInfo(this MonoBehaviour mono, string message)
        {
            Debug.Log(string.Format("<color=green>{0}</color>", message), mono);
        }
    }
}
