using System.Collections.Generic;
using System.Linq;
using System;

namespace UnityLibrary.Extensions
{
    public static class CollectionExtensions
    {
        public static T Random<T>(this IEnumerable<T> collection)
        {
            int count = UnityEngine.Random.Range(0, collection.Count());
            return collection.ElementAtOrDefault(count);
        }

        public static T Random<T>(this IEnumerable<T> collection, System.Random random)
        {
            int count = random.Next(0, collection.Count());
            return collection.ElementAtOrDefault(count);
        }

        public static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc)
        {
            return Random(collection, sumGetterFunc, () => UnityEngine.Random.Range(0f, 1f));
        }

        public static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc, System.Random rnd)
        {
            return Random(collection, sumGetterFunc, () => (float) rnd.NextDouble());
        }

        private static T Random<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc, Func<float> rnd01Func)
        {

            float r = rnd01Func() * Sum(collection, element => sumGetterFunc(element));
            float sum = 0;
            foreach (var element in collection)
            {
                var value = sumGetterFunc(element);
                if (r <= sum + value)
                    return element;
                sum += value;
            }
            return default(T);
        }

        /// <summary>
        /// Use this function instead of System.Linq.Sum.
        /// </summary>
        public static float Sum<T>(this IEnumerable<T> collection, Func<T, float> sumGetterFunc)
        {
            float sum = 0;
            foreach (var element in collection)
            {
                sum += sumGetterFunc(element);
            }
            return sum;
        }

        /// <summary>
        /// Use this function instead of System.Linq.Sum.
        /// </summary>
        public static int Max<T>(this IEnumerable<T> collection, Func<T, int> maxGetterFunc)
        {
            int max = int.MinValue;
            foreach (var element in collection)
            {
                var value = maxGetterFunc(element);
                if (value > max)
                    max = value;
            }
            return max;
        }

        public static int IndexOf<T>(this IEnumerable<T> collection, T element)
        {
            return collection.ToList().IndexOf(element);
        }
    }
}
