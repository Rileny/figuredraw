﻿using System;
using System.Collections;
using UnityEngine;
using UnityLibrary.Additions;

namespace UnityLibrary.Loaders
{
    /// <summary>
    /// Load something thorugth WWW class
    /// </summary>
    public sealed class MonoLoader : MonoBehaviour, ILoader
    {
        public event Action<MonoLoader> Destroyed = delegate { };

        private void OnDestroy()
        {
            StopAllCoroutines();
            EventSender.SendEvent(Destroyed, this);
        }

        public bool ReadyToLoad
        {
            get { return !gameObject.activeInHierarchy; }
        }

        #region ILoader

        void ILoader.Load(WWW www, Action<WWW> onReady)
        {
            gameObject.SetActive(true);
            StartCoroutine(LoadingProcess(www, onReady));
        }

        #endregion

        private IEnumerator LoadingProcess(WWW www, Action<WWW> onReady)
        {
            Debug.Log(string.Format("MonoLoader: {0} start loading from {1}...", name, www.url));
            yield return www;
            Debug.Log(string.Format("MonoLoader: {0}  loading finished from {1}!", name, www.url));
            EventSender.SendEvent(onReady, www);
            gameObject.SetActive(false);
        }
    }
}
