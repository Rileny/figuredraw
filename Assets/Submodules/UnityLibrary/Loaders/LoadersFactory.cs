﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityLibrary.Extensions;

namespace UnityLibrary.Loaders
{
    public sealed class LoadersFactory
    {
        #region Fields

        private readonly List<MonoLoader> loaders = new List<MonoLoader>();

        private Transform monoLoadersParent = null;
        private const string MONO_LOADERS_PARENT_NAME = "_MonoLoaders";

        #endregion

        #region Singletone

        private static LoadersFactory instance = null;

        public static LoadersFactory Instance
        {
            get
            {
                if (instance == null)
                    instance = new LoadersFactory();
                return instance;
            }
        }

        private LoadersFactory()
        {
            CreateMonoParent();
        }

        #endregion

        #region Functions

        public ILoader GetLoader()
        {
            var loader = loaders.FirstOrDefault(l => l.ReadyToLoad);
            if (loader == null)
                loader = CreteMonoLoader();
            return loader;
        }

        private void CreateMonoParent()
        {
            if (monoLoadersParent == null)
            {
                var go = GameObject.Find(MONO_LOADERS_PARENT_NAME);
                if (go == null)
                    go = new GameObject(MONO_LOADERS_PARENT_NAME);

                monoLoadersParent = go.transform;
                monoLoadersParent.ResetGlobal();
            }

            loaders.Clear();
            loaders.AddRange(monoLoadersParent.GetComponentsInChildren<MonoLoader>());
        }

        private MonoLoader CreteMonoLoader()
        {
            if (monoLoadersParent == null)
                CreateMonoParent();

            if (monoLoadersParent != null)
            {
                var loaderName = string.Format("Loader_{0}", monoLoadersParent.childCount);
                var mbl = (new GameObject(loaderName)).AddComponent<MonoLoader>();
                mbl.transform.parent = monoLoadersParent;
                mbl.transform.ResetLocal();
                mbl.Destroyed += OnMonoLoaderDestroyed;
                loaders.Add(mbl);
                return mbl;
            }

            Debug.LogWarning("LoadersFactory: failed to create loader due to null monoLoadersParent!");
            return null;
        }

        private void OnMonoLoaderDestroyed(MonoLoader monoLoader)
        {
            if (monoLoader != null)
            {
                monoLoader.Destroyed -= OnMonoLoaderDestroyed;
                loaders.Remove(monoLoader);
            }
        }

        #endregion
    }
}
