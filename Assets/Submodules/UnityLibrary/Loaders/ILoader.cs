﻿using System;
using UnityEngine;

namespace UnityLibrary.Loaders
{
    /// <summary>
    /// This needed for hide corotine from user
    /// </summary>
    public interface ILoader
    {
        /// <summary>
        /// Load someting from the WWW class, and return complete.
        /// </summary>
        void Load(WWW www, Action<WWW> onReady);
    }
}
