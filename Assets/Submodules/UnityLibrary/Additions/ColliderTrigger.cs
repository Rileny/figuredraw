using UnityEngine;
using System;
using UnityLibrary.Extensions;

namespace UnityLibrary.Additions
{
  public sealed class ColliderTrigger : MonoBehaviour
  {
    #region FIELDS

    public event Action<GameObject> TriggerEnter;
    public event Action<GameObject> TriggerExit;

    #endregion FIELDS

    #region UNITY EVENTS

    private void Awake()
    {
      if (this.Check("collider", GetComponent<Collider>() != null, LogType.Warning))
        GetComponent<Collider>().isTrigger = true;
    }

    private void OnTriggerEnter(Collider other)
    {
      EventSender.SendEvent(TriggerEnter, other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
      EventSender.SendEvent(TriggerExit, other.gameObject);
    }

    #endregion UNITY EVENTS
  }
}