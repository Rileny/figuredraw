﻿using UnityEngine;

namespace UnityLibrary.Additions
{
  public abstract class OptimizedMonoBehaviour : MonoBehaviour
  {

    private Transform cachedTransform = null;
    private Renderer cachedRenderer = null;
    private Renderer[] cachedRenderers = null;
    private Collider cachedCollider = null;
    private Collider2D cachedCollider2D = null;
    private Rigidbody cachedRigidbody = null;


    public new Transform transform
    {
      get
      {
        if (cachedTransform == null)
          cachedTransform = base.transform;
        return cachedTransform;
      }
    }

    public new Renderer renderer
    {
      get
      {
        if (cachedRenderer == null)
          cachedRenderer = base.GetComponent<Renderer>();
        return cachedRenderer;
      }
    }

    public Renderer[] CachedRenderers
    {
      get
      {
        if (cachedRenderers == null)
          cachedRenderers = GetComponentsInChildren<Renderer>();

        return cachedRenderers;
      }
    }

    public new Collider collider
    {
      get
      {
        if (cachedCollider == null)
          cachedCollider = base.GetComponent<Collider>();
        return cachedCollider;
      }
    }

    public new Collider2D collider2D
    {
      get
      {
        if (cachedCollider2D == null)
          cachedCollider2D = base.GetComponent<Collider2D>();
        return cachedCollider2D;
      }
    }

    public new Rigidbody rigidbody
    {
        get
        {
            if (cachedRigidbody == null)
                cachedRigidbody = base.GetComponent<Rigidbody>();
            return cachedRigidbody;
        }
    }
  }
}