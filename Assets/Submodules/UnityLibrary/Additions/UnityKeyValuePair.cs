﻿using System;
using UnityEngine;

namespace UnityLibrary.Additions
{
  [Serializable]
  public class UnityKeyValuePair<T1, T2>
  {
    [SerializeField] private T1 key;
    [SerializeField] private T2 val;

    public T1 Key
    {
      get { return key; }
      set { key = value; }
    }

    public T2 Value
    {
      get { return val; }
      set { val = value; }
    }

    public UnityKeyValuePair() : this (default(T1), default(T2))
    {
    }

    public UnityKeyValuePair(T1 key, T2 value)
    {
      this.key = key;
      this.val = value;
    }
  }
}
