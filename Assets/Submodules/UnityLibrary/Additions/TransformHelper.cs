﻿using UnityEngine;
using System.Collections;

namespace UnityLibrary.Additions
{
  public static class TransformHelper
  {
    #region Set parent

    public static void SetParent<T>(T mono, Transform parent) where T : MonoBehaviour
    {
      if (mono != null && parent != null)
        SetParent(mono.transform, parent);
    }

    public static void SetParent(Transform transform, Transform parent)
    {
      if (transform != null && parent != null)
        transform.parent = parent;
    }

    #endregion Set parent
  }
}