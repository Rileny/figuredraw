﻿using UnityEngine;
using System;
using UnityLibrary.Extensions;

namespace UnityLibrary.Additions
{
  public sealed class ColliderTrigger2D : MonoBehaviour
  {
    #region FIELDS

    public event Action<GameObject> TriggerEnter;
    public event Action<GameObject> TriggerExit;

    #endregion FIELDS

    #region UNITY EVENTS

    private void Awake()
    {
      if (this.Check("collider2D", GetComponent<Collider2D>() != null, LogType.Warning))
        GetComponent<Collider2D>().isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
      EventSender.SendEvent(TriggerEnter, other.gameObject);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
      EventSender.SendEvent(TriggerExit, other.gameObject);
    }

    #endregion UNITY EVENTS
  }
}