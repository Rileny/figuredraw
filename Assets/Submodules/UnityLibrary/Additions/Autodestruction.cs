using UnityEngine;

namespace UnityLibrary.Additions
{
  public sealed class Autodestruction : MonoBehaviour
  {
    [SerializeField] private bool useTimeOut = false;
    [SerializeField] private float time = 0;

    private void Awake()
    {
      if (useTimeOut)
        Destroy(gameObject, time > 0 ? time : 0);
    }

    public void Destruction()
    {
      Destroy(gameObject);
    }
  }
}