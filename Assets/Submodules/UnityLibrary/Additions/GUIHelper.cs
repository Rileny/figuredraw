﻿using UnityEngine;
using UnityEngine.UI;

namespace UnityLibrary.Additions
{
    public static class GUIHelper
    {
        public static void SetText(Text text, object message)
        {
            if (text != null)
                text.text = message.ToString();
        }

        public static void SetImageSprite(Image image, Sprite sprite)
        {
            if (image != null)
                image.sprite = sprite;
        }

        public static void SetImageColor(Image image, Color color)
        {
            if (image != null)
                image.color = color;
        }

        public static void SetButtonInteractable(Button button, bool interactable)
        {
            if (button != null)
                button.interactable = interactable;
        }

        public static void SetSlider(UnityEngine.UI.Slider slider, float value)
        {
            if (slider != null)
                slider.value = value;
        }
    }
}
