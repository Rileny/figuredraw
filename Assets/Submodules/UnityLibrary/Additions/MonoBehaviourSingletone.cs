﻿using UnityEngine;

namespace UnityLibrary.Additions
{
  public abstract class MonoBehaviourSingletone<T> : MonoBehaviour where T : MonoBehaviour
  {
    private static T instance = null;

    public static T Instance
    {
      get
      {
        if (instance == null)
        {
          instance = FindObjectOfType<T>();

          if (instance != null)
            return instance;

          var name = typeof(T).Name;
          Debug.Log(string.Format("Adding new {0} singleton to the scene.", name));
		      var go = new GameObject(name);
          instance = go.AddComponent<T>();
        }

        return instance;
      }
    }

    protected void InitSingletone(T inst)
    {
      if (instance == null)
      {
        instance = inst;
      }
      else if (instance != inst)
      {
        Debug.LogWarning(string.Format("MonoBehaviourSingletone: {0} singleton already exists! Destroying new one", name));

        if (Application.isPlaying)
          Destroy(gameObject);
        else
          DestroyImmediate(gameObject);
      }
    }
  }
}