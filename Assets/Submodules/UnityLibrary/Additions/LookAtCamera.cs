﻿using UnityEngine;

namespace UnityLibrary.Additions
{
  public sealed class LookAtCamera : OptimizedMonoBehaviour
  {
    [SerializeField] private Camera targetCamera = null;

    private void Awake()
    {
      if (targetCamera == null)
        targetCamera = Camera.main;

    }

    private void Update()
    {
      SetOrientation();
    }

    [ContextMenu("Set orientation")]
    private void SetOrientation()
    {
      if (targetCamera != null)
        transform.LookAt(targetCamera.transform);
    }

  }
}