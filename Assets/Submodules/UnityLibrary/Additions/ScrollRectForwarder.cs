﻿#if UNITY_4_6
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityLibrary.Additions
{
    /// <summary>
    /// Handle events to the scroll rect
    /// </summary>
    public sealed class ScrollRectForwarder : MonoBehaviour, IScrollHandler, IDragHandler, IBeginDragHandler,
        IEndDragHandler, IInitializePotentialDragHandler, IPointerClickHandler
    {
        /// <summary>
        /// Scroll rect for handling events
        /// </summary>
        [SerializeField] private ScrollRect scrollRect = null;

        /// <summary>
        /// Button for handling clicks
        /// </summary>
        [SerializeField] private Button button = null;

        private bool inScroll = false; //TODO This bool is fucking bool =) Try to fix it or try somethig else!
        private Button.ButtonClickedEvent onClick = null;

        private void Awake()
        {
            if (button != null)
            {
                onClick = button.onClick;
                button.onClick = new Button.ButtonClickedEvent();
            }
        }

        public void OnScroll(PointerEventData eventData)
        {
            inScroll = true;
            scrollRect.OnScroll(eventData);
        }

        public void OnDrag(PointerEventData eventData)
        {
            inScroll = true;
            scrollRect.OnDrag(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            inScroll = true;
            scrollRect.OnBeginDrag(eventData);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            inScroll = false;
            scrollRect.OnEndDrag(eventData);
        }

        public void OnInitializePotentialDrag(PointerEventData eventData)
        {
            scrollRect.OnInitializePotentialDrag(eventData);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (!inScroll && onClick != null)
                onClick.Invoke();
        }
    }
}

#endif
