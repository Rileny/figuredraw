﻿#if UNITY_4_6
using UnityEngine;

namespace UnityLibrary.Additions
{
    public sealed class GraphicIgnoreRaycast : MonoBehaviour, ICanvasRaycastFilter 
    {
        public bool IsRaycastLocationValid(Vector2 screenPoint, Camera eventCamera)
        {
            return false;
        } 
    }
}
#endif
