﻿using System.Text.RegularExpressions;
using UnityEngine;

namespace UnityLibrary.Additions
{

#if UNITY_EDITOR
  using System.Collections.Generic;
  using System.Linq;
  using UnityEditor;
#endif

  [ExecuteInEditMode]
  public sealed class Identifier : MonoBehaviour
  {
    [SerializeField] private string id;

    public string Id
    {
      get
      {
        return id;
      }
    }

    private void Awake()
    {
      if (IsIdValid)
        return;

      ResetId();

#if UNITY_EDITOR
      if (!Application.isPlaying)
        IdentifyProjectIdentifiers(false);
#endif
    }

    private static string GenerateId()
    {
      return System.Guid.NewGuid().ToString();
    }

    public static bool IsGuid(string guid)
    {
      if (string.IsNullOrEmpty(guid) || guid == System.Guid.Empty.ToString())
        return false;

      var guidPattern = new Regex("[a-fA-F0-9]{8}(-[a-fA-F0-9]{4}){3}-[a-fA-F0-9]{12}");
      return guidPattern.IsMatch(guid);
    }

    public bool IsIdValid
    {
      get { return IsGuid(id); }
    }

    private void ResetId()
    {
      string prevId = id;

      id = GenerateId();
      if (string.IsNullOrEmpty(prevId))
        Debug.Log(string.Format("'{0}' Identifier changed ID value to '{1}'", name, id));
      else
        Debug.LogWarning(string.Format("'{0}' Identifier changed ID value '{1}' to '{2}'", name, prevId, id));
    }

#if UNITY_EDITOR

    public void ResetIdentifier()
    {
      ResetId();
    }

    public void Identify()
    {
      if (IsIdValid)
        return;

      ResetIdentifier();
    }

    public void ClearIdentifier()
    {
      id = string.Empty;
      Debug.Log(string.Format("'{0}' Identifier changed ID to empty", name));
    }

    public static void IdentifySceneIdentifiers(bool reset)
    {
      var identifiedMonos = GameObject.FindObjectsOfType(typeof (Identifier)).Cast<Identifier>();
      if (identifiedMonos != null && identifiedMonos.Any())
      {
        var prefabs =
          Resources.FindObjectsOfTypeAll(typeof (Identifier))
            .Cast<Identifier>()
            .Where(i => i.IsIdValid && !identifiedMonos.Contains(i));

        List<string> ids = new List<string>();

        foreach (var p in prefabs)
          if (p != null && p.IsIdValid)
            ids.Add(p.Id);

        foreach (var identifiedMono in identifiedMonos)
        {
          if (reset || (identifiedMono.IsIdValid && ids.Contains(identifiedMono.Id)))
            identifiedMono.ResetIdentifier();
          else
            identifiedMono.Identify();

          ids.Add(identifiedMono.Id);

          EditorUtility.SetDirty(identifiedMono);
        }
      }

      Debug.Log("Scene identifiers identified");
    }

    public static void IdentifyProjectIdentifiers(bool reset)
    {
      var sceneIdentifiers = GameObject.FindObjectsOfType(typeof (Identifier));
      var prefabs = Resources.FindObjectsOfTypeAll(typeof (Identifier)).Except(sceneIdentifiers).Cast<Identifier>();

      var ids = new List<string>();

      foreach (var p in prefabs)
        if (p != null && p.IsIdValid)
          ids.Add(p.Id);

      foreach (var p in prefabs)
      {
        if (p.IsIdValid && ids.Where(id => id == p.Id).Count() > 1)
        {
          ids.Remove(p.Id);
          p.ResetIdentifier();
        }
        else if (reset)
          p.ResetIdentifier();
        else
          p.Identify();

        ids.Add(p.Id);

        EditorUtility.SetDirty(p);
      }

      Debug.Log("Project identifiers identified");
    }
#endif
  }
}