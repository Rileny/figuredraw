using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityLibrary.Extensions;

namespace UnityLibrary.Additions
{
  public static class GameObjectHelper
  {
    #region Find components

    public static T GetComponentAt<T>(Vector2 screenPosition, int layer) where T : Component
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to get component due to null main camera!");
        return null;
      }

      RaycastHit hit = new RaycastHit();
      var ray = Camera.main.ScreenPointToRay(screenPosition);
      if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << layer))
        return hit.transform.GetComponent<T>();

      return null;
    }

    public static T GetComponentAt<T>(Vector2 screenPosition) where T : Component
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to get component due to null main camera!");
        return null;
      }

      RaycastHit hit = new RaycastHit();
      var ray = Camera.main.ScreenPointToRay(screenPosition);
      if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        return hit.transform.GetComponent<T>();

      return null;
    }

    public static T GetGenericComponentAt<T>(Vector2 screenPosition, int layer) where T : class
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to get component due to null main camera!");
        return null;
      }

      RaycastHit hit = new RaycastHit();
      var ray = Camera.main.ScreenPointToRay(screenPosition);
      if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << layer))
        return hit.transform.gameObject.GetGenericComponent<T>();

      return null;
    }

    public static T GetGenericComponentAt<T>(Vector2 screenPosition) where T : class
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to get component due to null main camera!");
        return null;
      }

      RaycastHit hit = new RaycastHit();
      var ray = Camera.main.ScreenPointToRay(screenPosition);
      if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        return hit.transform.gameObject.GetGenericComponent<T>();

      return null;
    }

    public static Vector3 ScreenToWorld(Vector2 screenPosition, int layer)
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to covert screen to world due to null main camera!");
        return Vector3.zero;
      }

      var ray = Camera.main.ScreenPointToRay(screenPosition);
      RaycastHit hit;
      if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << layer))
        return hit.point;
      return Vector3.zero;
    }

    public static Vector3 ScreenToWorld(Vector2 screenPosition)
    {
      if (Camera.main == null)
      {
        Debug.LogWarning("Failed to covert screen to world due to null main camera!");
        return Vector3.zero;
      }

      var ray = Camera.main.ScreenPointToRay(screenPosition);
      RaycastHit hit;
      if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        return hit.point;
      return Vector3.zero;
    }

    #endregion

    #region Set active

    public static void SetActive<T>(T mono, bool active) where T : MonoBehaviour
    {
      if (mono != null)
        SetActive(mono.gameObject, active);
    }

    public static void SetActive(GameObject gameObject, bool active)
    {
      if (gameObject != null)
        gameObject.SetActive(active);
    }

    #endregion

    #region Find

    public static IEnumerable<T> FindObjectsOfType<T>() where T : Component
    {
      return GameObject.FindObjectsOfType(typeof (T)).Cast<T>();
    }

    public static Camera FindCameraForLayer(int layer)
    {
      int layerMask = 1 << layer;

      Camera[] cameras = FindActive<Camera>();

      for (int i = 0, imax = cameras.Length; i < imax; ++i)
      {
        Camera cam = cameras[i];

        if ((cam.cullingMask & layerMask) != 0)
        {
          return cam;
        }
      }
      return null;
    }

    public static T[] FindActive<T>() where T : Component
    {
#if UNITY_3_5 || UNITY_4_0
		return GameObject.FindSceneObjectsOfType(typeof(T)) as T[];
#else
      return GameObject.FindObjectsOfType(typeof (T)) as T[];
#endif
    }

    public static T FindNearest<T>(this IEnumerable<T> objects, Transform transform) where T : Component
    {
      T result = null;
      float distance = float.MaxValue;
      foreach (var obj in objects)
      {
        float d = Vector3.Distance(obj.transform.position, transform.position);
        if (result == null || d < distance)
        {
          result = obj;
          distance = d;
        }
      }
      return result;
    }

    /// <summary>
    /// Its usefull for find interface component. Slow function.
    /// </summary>
    public static T FindGenericComponent<T>() where T : class
    {
      return FindGenericComponents<T>().FirstOrDefault();
    }

    /// <summary>
    /// Its usefull for find interface components. Slow function.
    /// </summary>
    public static IEnumerable<T> FindGenericComponents<T>() where T : class
    {
      return Object.FindObjectsOfType<Component>().OfType<T>();
    }

    #endregion
  }
}