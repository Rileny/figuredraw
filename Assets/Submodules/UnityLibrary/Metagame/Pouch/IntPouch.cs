﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityLibrary.Additions;

namespace UnityLibrary.Metagame.Pouch
{
    /// <summary>
    /// Functional of pouch with integer values
    /// </summary>
    public class IntPouch : IPouch<int>
    {
        #region Fields

        protected sealed class IntPouchResource
        {
            private int id = -1;
            private int count = 0;
            private int? maxCount = null;

            public int Id
            {
                get { return id; }
            }

            public int Count
            {
                get { return count; }
                set { count = value; }
            }

            public int? MaxCount
            {
                get { return maxCount; }
                set { maxCount = value; }
            }

            /// <summary>
            /// Create resource data with infine max value
            /// </summary>
            public IntPouchResource(int id, int count) : this(id, count, null)
            {
            }

            /// <summary>
            /// Create simple resource data
            /// </summary>
            public IntPouchResource(int id, int count, int? maxCount)
            {
                this.id = id;
                this.count = count;
                this.maxCount = maxCount;
            }
        }

        /// <summary>
        /// All resources in pouch.
        /// NOTE: For Dictionary iOS can crush
        /// </summary>
        protected readonly List<IntPouchResource> resources = new List<IntPouchResource>();

        private event Action<int, int> ResourceCountChanged = delegate { };
        private event Action<int, int?> ResourceMaxCountChanged = delegate { };
        private event Action<int, int> ResourceLack = delegate { };
        private event Action<int, int> ResourceAtMax = delegate { };

        #endregion

        #region Constructors

        /// <summary>
        /// Create instanse of empty pouch
        /// without any resource there 
        /// </summary>
        public IntPouch()
        {
        }

        /// <summary>
        /// Create pouch with infinite resource max values
        /// </summary>
        public IntPouch(IEnumerable<int> resourceIds)
        {
            foreach (var id in resourceIds)
                AddResource(id, 0);
        }

        /// <summary>
        /// Create pouch with infinite resources max values and start resource count
        /// </summary>
        public IntPouch(IEnumerable<UnityKeyValuePair<int, int>> resources)
        {
            foreach (var pair in resources)
                AddResource(pair.Key, pair.Value);
        }

        /// <summary>
        /// Create pouch with fixed resources max values and start resource count
        /// </summary>
        public IntPouch(IEnumerable<UnityKeyValuePair<int, int>> resources,
            IEnumerable<UnityKeyValuePair<int, int?>> maxValues)
        {
            foreach (var pair in resources)
            {
                var mvp = maxValues.FirstOrDefault(p => p.Key == pair.Key);
                if (mvp != null)
                    AddResource(pair.Key, pair.Value, mvp.Value);
                else
                    AddResource(pair.Key, pair.Value);
            }
        }

        #endregion

        #region Functions

        /// <summary>
        /// Add new resource or set new resource data if
        /// resource already exist
        /// </summary>
        protected void AddResource(IntPouchResource resource)
        {
            AddResource(resource.Id, resource.Count, resource.MaxCount);
        }

        /// <summary>
        /// Add new resource or set new resource data if
        /// resource already exist
        /// </summary>
        protected void AddResource(int id, int count, int? maxCount = null)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
            {
                resource.Count = count;
                resource.MaxCount = maxCount;
                EventSender.SendEvent(ResourceCountChanged, resource.Id, resource.Count);
            }
            else
            {
                resources.Add(new IntPouchResource(id, count, maxCount));
                EventSender.SendEvent(ResourceCountChanged, id, count);
            }
        }

        #endregion

        #region IPouch

        event Action<int, int> IPouch<int>.ResourceCountChanged
        {
            add { ResourceCountChanged += value; }
            remove { ResourceCountChanged -= value; }
        }

        event Action<int, int?> IPouch<int>.ResourceMaxCountChanged
        {
            add { ResourceMaxCountChanged += value; }
            remove { ResourceMaxCountChanged -= value; }
        }

        event Action<int, int> IPouch<int>.ResourceLack
        {
            add { ResourceLack += value; }
            remove { ResourceLack -= value; }
        }

        event Action<int, int> IPouch<int>.ResourceAtMax
        {
            add { ResourceAtMax += value; }
            remove { ResourceAtMax -= value; }
        }

        IEnumerable<int> IPouch<int>.ResourcesIds
        {
            get { return resources.Select(i=>i.Id); }
        } 

        bool IPouch<int>.TryIncreaseResource(int id, int count)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
            {
                if (resource.MaxCount.HasValue)
                {
                    int freeSpace = resource.MaxCount.Value - resource.Count;
                    if (freeSpace - count >= 0)
                    {
                        resource.Count += count;
                        EventSender.SendEvent(ResourceCountChanged, resource.Id, resource.Count);
                        return true;
                    }

                    resource.Count = resource.MaxCount.Value;
                    EventSender.SendEvent(ResourceCountChanged, resource.Id, resource.Count);
                    EventSender.SendEvent(ResourceAtMax, resource.Id, count - freeSpace);
                    return false;
                }

                resource.Count += count;
                EventSender.SendEvent(ResourceCountChanged, resource.Id, resource.Count);
                return true;
            }

            Debug.LogWarning(string.Format("IntPouch: resource {0} isn't exest in pouch!", id));
            return false;
        }

        bool IPouch<int>.TryDecreaseResource(int id, int count)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
            {
                if (resource.Count >= count)
                {
                    resource.Count -= count;
                    EventSender.SendEvent(ResourceCountChanged, resource.Id, resource.Count);
                    return true;
                }

                EventSender.SendEvent(ResourceLack, resource.Id, count - resource.Count);
                return false;
            }

            Debug.LogWarning(string.Format("IntPouch: resource {0} isn't exest in pouch!", id));
            return false;
        }

        void IPouch<int>.AddResource(int id)
        {
            if (resources.All(r => r.Id != id))
                resources.Add(new IntPouchResource(id, 0));
        }

        bool IPouch<int>.HasResource(int id)
        {
            return resources.Any(r => r.Id == id);
        }

        void IPouch<int>.RemoveResource(int id)
        {
            resources.RemoveAll(r => r.Id == id);
        }

        int IPouch<int>.GetResource(int id)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
                return resource.Count;
            return 0;
        }

        int? IPouch<int>.GetMaxResourceCount(int id)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
                return resource.MaxCount;
            return 0;
        }

        void IPouch<int>.SetMaxResourceCount(int id, int? maxCount)
        {
            var resource = resources.FirstOrDefault(r => r.Id == id);
            if (resource != null)
            {
                resource.MaxCount = maxCount;
                EventSender.SendEvent(ResourceMaxCountChanged, resource.Id, resource.MaxCount);
            }
        }

        void IPouch<int>.ClearResourcesOnly()
        {
            foreach (var r in resources)
            {
                r.Count = 0;
                EventSender.SendEvent(ResourceCountChanged, r.Id, r.Count);
            }
        }

        void IPouch<int>.Clear()
        {
            resources.Clear();
        }

        #endregion

    }
}
