﻿using System;
using System.Collections.Generic;

namespace UnityLibrary.Metagame.Pouch
{
    /// <summary>
    /// Describe functional of player Pouch.
    /// It can contain player resources by its id and value.
    /// </summary>
    public interface IPouch<TCount> where TCount : struct
    {
        /// <summary>
        /// Send when resource amount is changed.
        /// As resource amount return actual resource value.
        /// </summary>
        event Action<int, TCount> ResourceCountChanged;

        /// <summary>
        /// Send when resource max count is changed.
        /// As resource amount return actual resource max value.
        /// </summary>
        event Action<int, TCount?> ResourceMaxCountChanged;

        /// <summary>
        /// Send when resource can't be decreased.
        /// As resource amount return lack resource value.
        /// </summary>
        event Action<int, TCount> ResourceLack;

        /// <summary>
        /// Send when resource can't be increased.
        /// As resource amount return superfluous resource value.
        /// </summary>
        event Action<int, TCount> ResourceAtMax;

        /// <summary>
        /// Returns ids of all resources
        /// </summary>
        IEnumerable<int> ResourcesIds { get; } 

        /// <summary>
        /// Increase resource and return true if success
        /// </summary>
        bool TryIncreaseResource(int id, TCount count);

        /// <summary>
        /// Decrease resource and return true if success
        /// </summary>
        bool TryDecreaseResource(int id, TCount count);

        /// <summary>
        /// Add new resource to pouch
        /// </summary>
        void AddResource(int id);

        /// <summary>
        /// Return true if pouch contains
        /// resource with current id
        /// </summary>
        bool HasResource(int id);

        /// <summary>
        /// Remove resource from pouch
        /// </summary>
        void RemoveResource(int id);

        /// <summary>
        /// Return actual amout of resource
        /// </summary>
        TCount GetResource(int id);

        /// <summary>
        /// Return max resource count. If null - resource is infinite
        /// </summary>
        TCount? GetMaxResourceCount(int id);

        /// <summary>
        /// Set max resource count. If null - resource is infinite
        /// </summary>
        void SetMaxResourceCount(int id, TCount? maxCount);

        /// <summary>
        /// Remove only resources values
        /// </summary>
        void ClearResourcesOnly();

        /// <summary>
        /// Remove all data from pouch resources ids and its values
        /// </summary>
        void Clear();
    }
}
