﻿using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;
using UnityEngine;
using System.Linq;

namespace EJaw.UnityDatabase
{
    public sealed class RuntimeUnityDatabase : IRuntimeUnityDatabase
    {
        private readonly ITable<TableRecord> tablesDatabase = null;

        #region Constructors

        public RuntimeUnityDatabase(ITable<TableRecord> tablesDatabase)
        {
            if (tablesDatabase == null)
            {
                Debug.LogError("RuntimeUnityDatabase: invalid value of Tables Database");
                return;
            }

            this.tablesDatabase = tablesDatabase;
        }

        #endregion

        #region Functions

        private ITable<TRecord> GetTable<TRecord>() where TRecord : IRecord
        {
            if (tablesDatabase == null) return null;

            ITable<TRecord> t = null;
            foreach (var r in tablesDatabase.Records)
            {
                if (r == null || r.Table == null) continue;
                if (r.Table.RecordType == typeof (TRecord))
                    t = r.Table as ITable<TRecord>;
            }

            return t;
        }

        #endregion

        #region IRuntimeUnityDatabase

        ITable<TRecord> IRuntimeUnityDatabase.GetTable<TRecord>()
        {
            return GetTable<TRecord>();
        }

        TRecord IRuntimeUnityDatabase.GetRecord<TRecord>(int recordId)
        {
            var t = GetTable<TRecord>();
            if (t != null)
                return t.GetRecord(recordId);
            return default(TRecord);
        }

        TTable IRuntimeUnityDatabase.GetTableDirectly<TTable>()
        {
            if (tablesDatabase == null) return null;
            return tablesDatabase.Records.Select(r => r.Table).OfType<TTable>().FirstOrDefault();
        }

        #endregion
    }
}
