﻿using System;
using UnityEngine;

namespace EJaw.UnityDatabase.Tables
{
    /// <summary>
    /// This class needed to select all tables classes from other scriptable objects 
    /// </summary>
    public abstract class ScriptableObjectTable : ScriptableObject
    {
        /// <summary>
        /// Return table's recod type
        /// </summary>
        public abstract Type RecordType { get; }
    }
}
