﻿using EJaw.UnityDatabase.Records;

namespace EJaw.UnityDatabase.Tables
{
    /// <summary>
    /// This table contains referece on all others
    /// project tables
    /// </summary>
    public sealed class TablesDatabase : BaseTable<TableRecord>
    {
#if UNITY_EDITOR

        protected override void RecordsHeaders(EditorClasses.UnityDatabase.Layouts.ITableLayout layout)
        {
            layout.Caption("Name");
            layout.Caption("Table");
        }

        protected override void RecordData(EditorClasses.UnityDatabase.Layouts.ITableLayout layout, TableRecord record)
        {
            var n = (record.Table != null) ? record.Table.name : "Unsigned";
            layout.Caption(string.Format("{0}. {1}", (record.Id + 1), n));
            record.Table = layout.Field(record.Table);
        }

#endif

    }
}
