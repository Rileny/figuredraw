﻿using System.Collections.Generic;
using System.Linq;

namespace EJaw.UnityDatabase
{
    /// <summary>
    /// This class generate identifiers for records
    /// </summary>
    public static class IdGenerator
    {
        public const int INVALID_ID = -1;

#if UNITY_EDITOR

        public static int GetUniqueIdSmart(IEnumerable<int> ids)
        {
            if (ids == null)
                return INVALID_ID;

            var list = ids.ToList();
            if (list.Count == 0)
                return 0;

            list.Sort();
            if (list.Last() != list.Count - 1)
            {
                for (int i = 0; i < list.Count; ++i)
                {
                    if (i != list[i])
                        return i;
                }
            }
            return list.Count;
        }

#endif
    }
}
