﻿using EJaw.UnityDatabase.Tables;
using System;
using UnityEngine;

namespace EJaw.UnityDatabase.Records
{
    /// <summary>
    /// This record contain information about tables that will be used in project.
    /// </summary>
    [Serializable]
    public sealed class TableRecord : BaseRecord
    {
        #region Fields

        [SerializeField] private ScriptableObjectTable table = null;

        #endregion

        #region Properties

        public ScriptableObjectTable Table
        {
            get { return table; }
#if UNITY_EDITOR
            set { table = value; }
#endif
        }

        #endregion

        #region Constructors

        public TableRecord()
        {
        }

#if UNITY_EDITOR
        public TableRecord(ScriptableObjectTable table)
        {
            if (table == null)
                return;

            this.table = table;
        }
#endif

        #endregion
    }
}
