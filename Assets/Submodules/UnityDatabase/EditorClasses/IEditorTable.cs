﻿#if UNITY_EDITOR
using EJaw.EditorClasses.UnityDatabase.Layouts;
using EJaw.UnityDatabase.Records;
using System;

namespace EJaw.EditorClasses.UnityDatabase
{
    /// <summary>
    /// Describe editor table functional
    /// </summary>
    public interface IEditorTable<TRecord> where TRecord : IRecord, IEditorRecord
    {
        /// <summary>
        /// Add record at the end of table
        /// </summary>
        void Add(TRecord record);

        /// <summary>
        /// Add record after previousRecord
        /// </summary>
        void Add(TRecord record, TRecord previousRecord);

        /// <summary>
        /// Remove record from table by its id
        /// </summary>
        void Remove(int id);

        /// <summary>
        /// Remove record from table by its value
        /// </summary>
        void Remove(TRecord record);

        /// <summary>
        /// Sort table by setted compression
        /// </summary>
        void Sort(Comparison<TRecord> comparison);

        /// <summary>
        /// Return true if table contains record with id
        /// </summary>
        bool Contains(int id);

        /// <summary>
        /// Display headers for table data
        /// </summary>
        void TableHeaders(ITableLayout layout);

        /// <summary>
        /// Display table data
        /// </summary>
        void TableData(ITableLayout layout);

        /// <summary>
        /// Display headers for record data
        /// </summary>
        void RecordsHeaders(ITableLayout layout);

        /// <summary>
        /// Display data for current record
        /// </summary>
        void RecordData(ITableLayout layout, TRecord record);

        /// <summary>
        /// Move record to upper
        /// </summary>
        void SwapRecordUp(TRecord record);

        /// <summary>
        /// Move record to lower
        /// </summary>
        void SwapRecordDown(TRecord record);

    }
}

#endif
