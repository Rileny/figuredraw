﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Layouts
{
    public sealed class VerticalLayout : TableLayout, IWindowLayout
    {
        #region Counstuctors

        public VerticalLayout()
            : base()
        {
        }

        public VerticalLayout(float defaultSpace, float defaultWidth, float defaultHeight)
            : base(defaultSpace, defaultWidth, defaultHeight)
        {
        }

        #endregion

        #region TableLayout

        public override void BeginDrawArray()
        {
            EditorGUILayout.BeginHorizontal();
        }

        public override void EndDrawArray()
        {
            EditorGUILayout.EndHorizontal();
        }

        public override void BeginDrawIteration()
        {
            EditorGUILayout.BeginVertical();
        }

        public override void EndDrawIteration()
        {
            EditorGUILayout.EndVertical();
        }

        public override string DrawArrayElement(string value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override int DrawArrayElement(int value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override float DrawArrayElement(float value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override bool DrawArrayElement(bool value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override string DrawArrayElement(string current, IEnumerable<string> data, float width,
            float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(current, data, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override int DrawArrayElement(int value, IEnumerable<int> ids, IEnumerable<string> names,
            float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, ids, names, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override TUnityObject DrawArrayElement<TUnityObject>(TUnityObject value, float width,
            float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override Vector3 DrawArrayElement(Vector3 value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override Vector2 DrawArrayElement(Vector2 value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = Field(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        public override System.Enum DrawEnumArrayElement(System.Enum value, float width, float height)
        {
            EditorGUILayout.BeginHorizontal();
            var v = EnumField(value, width, height);
            EditorGUILayout.EndHorizontal();
            return v;
        }

        #endregion

        #region IWindowLayout

        void IWindowLayout.DrawTable<TRecord>(IEditorTable<TRecord> table)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(1f));

            EditorGUILayout.BeginVertical();
            table.TableHeaders(this);
            EditorGUILayout.EndVertical();

            EditorGUILayout.BeginVertical();
            table.TableData(this);
            EditorGUILayout.EndVertical();

            EditorGUILayout.EndHorizontal();
        }

        void IWindowLayout.DrawRecord<TTable, TRecord>(TTable table)
        {
            EditorGUILayout.BeginHorizontal(GUILayout.Width(1f));

            EditorGUILayout.BeginVertical();
            if (Button("New Record", Color.green, true, BigButtonWidth, defaultHeight))
            {
                AddRecordToTable<TTable, TRecord>(table);
            }
            table.RecordsHeaders(this);
            EditorGUILayout.EndVertical();


            foreach (var r in table.Records.ToList())
            {
                EditorGUILayout.BeginVertical();
                EditorGUILayout.BeginHorizontal();
                if (Button("-", Color.red, true, SmallButtonWidth, defaultHeight))
                {
                    table.Remove(r);
                    break;
                }
                var list = table.Records.ToList();
                if (Button("++", Color.magenta, true, SmallButtonWidth, defaultHeight))
                {
                    AddRecordToTable(table, r);
                }
                if (Button("<", Color.yellow, true, SmallButtonWidth, defaultHeight))
                {
                    if (list.IndexOf(r) != 0)
                    {
                        table.SwapRecordUp(r);
                        break;
                    }
                }
                if (Button(">", Color.cyan, true, SmallButtonWidth, defaultHeight))
                {
                    if (list.IndexOf(r) != list.Count - 1)
                    {
                        table.SwapRecordDown(r);
                        break;
                    }
                }

                EditorGUILayout.EndHorizontal();

                table.RecordData(this, r);

                EditorGUILayout.EndVertical();
            }

            EditorGUILayout.EndHorizontal();
        }

        #endregion
    }
}

#endif
