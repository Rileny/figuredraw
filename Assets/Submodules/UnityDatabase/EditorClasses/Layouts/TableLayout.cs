﻿#if UNITY_EDITOR
using EJaw.EditorClasses.UnityDatabase.Helpers;
using System.Reflection;
using UnityEditorInternal;
using EJaw.UnityDatabase;
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Layouts
{
    /// <summary>
    /// This class describe tables visualization in editor window
    /// </summary>
    public abstract class TableLayout : ITableLayout
    {
        #region Fields

        protected readonly float defaultSpace = 0f;
        protected readonly float defaultWidth = 175f;
        protected readonly float defaultHeight = 16f;

        #endregion

        #region Properties

        protected float SmallButtonWidth
        {
            get { return defaultWidth / 7f; }
        }

        protected float BigButtonWidth
        {
            get { return defaultWidth * 0.65f; }
        }


        #endregion

        #region Counstuctors

        protected TableLayout()
        {
        }

        protected TableLayout(float defaultSpace, float defaultWidth, float defaultHeight)
        {
            this.defaultSpace = defaultSpace;
            this.defaultWidth = defaultWidth;
            this.defaultHeight = defaultHeight;
        }

        #endregion

        #region ITableLayout

        public void Caption(string caption)
        {
            Caption(caption, defaultWidth, defaultHeight);
        }

        public void Caption(string caption, float width, float height)
        {
            EditorGUILayout.LabelField(caption, EditorStyles.boldLabel, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
        }

        public void ClickableCaption(string caption, Action onClick)
        {
            ClickableCaption(caption, onClick, defaultWidth, defaultHeight);
        }

        public void ClickableCaption(string caption, Action onClick, float width, float height)
        {
            bool button = GUILayout.Button(caption, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            if (button)
            {
                var h = onClick;
                if (h != null)
                    h();
            }
        }

        public string Field(string val)
        {
            return Field(val, defaultWidth, defaultHeight);
        }

        public string Field(string val, float width, float height)
        {
            var v = EditorGUILayout.TextField(val, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public int Field(int value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public int Field(int value, float width, float height)
        {
            var v = EditorGUILayout.IntField(value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public float Field(float value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public float Field(float value, float width, float height)
        {
            var v = EditorGUILayout.FloatField(value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public bool Field(bool value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public bool Field(bool value, float width, float height)
        {
            var v = EditorGUILayout.Toggle(value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public string Field(string current, IEnumerable<string> data)
        {
            return Field(current, data, defaultWidth, defaultHeight);
        }

        public string Field(string current, IEnumerable<string> data, float width, float height)
        {
            int index = data.ToList().IndexOf(current);
            index = EditorGUILayout.Popup(index, data.ToArray(), GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            if (index >= 0 && index < data.Count())
                return data.ElementAt(index);
            return current;
        }

        public int Field(int value, IEnumerable<int> ids, IEnumerable<string> names)
        {
            return Field(value, ids, names, defaultWidth, defaultHeight);
        }

        public int Field(int value, IEnumerable<int> ids, IEnumerable<string> names, float width, float height)
        {
            int index = ids.ToList().IndexOf(value);
            index = EditorGUILayout.Popup(index, names.ToArray(), GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            if (index >= 0 && index < ids.Count())
                return ids.ElementAt(index);
            return IdGenerator.INVALID_ID;
        }

        public Color Field(Color value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public Color Field(Color value, float width, float height)
        {
            var v = EditorGUILayout.ColorField(string.Empty, value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public LayerMask MaskField(int value)
        {
            Type internalEditorUtilityType = typeof(InternalEditorUtility);
            PropertyInfo sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
            var sortingLayers = (string[])sortingLayersProperty.GetValue(null, new object[0]);
            var sortingMasks = new List<LayerMask>();
            foreach (var layer in sortingLayers)
            {
                sortingMasks.Add(LayerMask.NameToLayer(layer));
            }
            return EditorGUILayout.MaskField(value, sortingLayers);
        }

        public TUnityObject Field<TUnityObject>(TUnityObject value)
            where TUnityObject : UnityEngine.Object
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public TUnityObject Field<TUnityObject>(TUnityObject value, float width, float height)
            where TUnityObject : UnityEngine.Object
        {
            var o =
                EditorGUILayout.ObjectField(value, typeof (TUnityObject), false, GetOptions(width, height)) as
                    TUnityObject;
            GUILayout.Space(defaultSpace);
            return o;
        }

        public Vector3 Field(Vector3 value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public Vector4 Field(Vector4 value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public Vector3 Field(Vector3 value, float width, float height)
        {
            var v = EditorGUILayout.Vector3Field(string.Empty, value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public Vector4 Field(Vector4 value, float width, float height)
        {
            var v = EditorGUILayout.Vector4Field(string.Empty, value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public Vector2 Field(Vector2 value)
        {
            return Field(value, defaultWidth, defaultHeight);
        }

        public Vector2 Field(Vector2 value, float width, float height)
        {
            var v = EditorGUILayout.Vector2Field(string.Empty, value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public System.Enum EnumField(System.Enum value)
        {
            return EnumField(value, defaultWidth, defaultHeight);
        }

        public System.Enum EnumField(System.Enum value, float width, float height)
        {
            var v = EditorGUILayout.EnumPopup(value, GetOptions(width, height));
            GUILayout.Space(defaultSpace);
            return v;
        }

        public abstract void BeginDrawArray();

        public abstract void EndDrawArray();

        public abstract void BeginDrawIteration();

        public abstract void EndDrawIteration();

        public string DrawArrayElement(string value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract string DrawArrayElement(string value, float width, float height);

        public int DrawArrayElement(int value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract int DrawArrayElement(int value, float width, float height);

        public float DrawArrayElement(float value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract float DrawArrayElement(float value, float width, float height);

        public bool DrawArrayElement(bool value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract bool DrawArrayElement(bool value, float width, float height);

        public string DrawArrayElement(string current, IEnumerable<string> data)
        {
            return DrawArrayElement(current, data, defaultWidth, defaultHeight);
        }

        public abstract string DrawArrayElement(string current, IEnumerable<string> data, float width, float height);

        public int DrawArrayElement(int value, IEnumerable<int> ids, IEnumerable<string> names)
        {
            return DrawArrayElement(value, ids, names, defaultWidth, defaultHeight);
        }

        public abstract int DrawArrayElement(int value, IEnumerable<int> ids, IEnumerable<string> names, float width,
            float height);

        public TUnityObject DrawArrayElement<TUnityObject>(TUnityObject value)
            where TUnityObject : UnityEngine.Object
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract TUnityObject DrawArrayElement<TUnityObject>(TUnityObject value, float width, float height)
            where TUnityObject : UnityEngine.Object;

        public Vector3 DrawArrayElement(Vector3 value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract Vector3 DrawArrayElement(Vector3 value, float width, float height);

        public Vector2 DrawArrayElement(Vector2 value)
        {
            return DrawArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract Vector2 DrawArrayElement(Vector2 value, float width, float height);

        public System.Enum DrawEnumArrayElement(System.Enum value)
        {
            return DrawEnumArrayElement(value, defaultWidth, defaultHeight);
        }

        public abstract System.Enum DrawEnumArrayElement(System.Enum value, float width, float height);

        public bool Button(string buttonName, Color buttonColor, bool buttonEnabled = true)
        {
            return Button(buttonName, buttonColor, buttonEnabled, defaultWidth, defaultHeight);
        }

        public bool Button(string buttonName, Color buttonColor, bool buttonEnabled, float width, float height)
        {
            return EditorHelper.Button(buttonName, buttonColor, buttonEnabled, GetOptions(width, height));
        }

        public void ExceptionCatch(string message, MessageType type = MessageType.Warning)
        {
            EditorGUILayout.HelpBox(message, MessageType.Warning);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Add record of type T2 to table of type T1
        /// Cause of this is non supported generic overriding
        /// functions in Unity3D
        /// </summary>   
        protected void AddRecordToTable<T1, T2>(T1 table)
            where T1 : ScriptableObjectTable, ITable<T2>, IEditorTable<T2>
            where T2 : IRecord, IEditorRecord, new()
        {
            var record = new T2();
            table.Add(record);
        }

        protected void AddRecordToTable<T1, T2>(T1 table, T2 prevRecord)
            where T1 : ScriptableObjectTable, ITable<T2>, IEditorTable<T2>
            where T2 : IRecord, IEditorRecord, new()
        {
            var record = new T2();
            table.Add(record, prevRecord);
        }

        private GUILayoutOption[] GetOptions(float width, float height)
        {
            return new[] {GUILayout.Width(width), GUILayout.Height(height)};
        }

        #endregion
    }
}

#endif
