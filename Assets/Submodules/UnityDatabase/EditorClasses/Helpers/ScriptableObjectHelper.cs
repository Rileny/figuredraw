#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Helpers
{
    /// <summary>
    /// Helper class for creation scriptable objects in project
    /// </summary>
    public static class ScriptableObjectHelper
    {
        public static T Create<T>(string path) where T : ScriptableObject
        {
            var so = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(so, path);
            return so;
        }
    }
}

#endif
