﻿#if UNITY_EDITOR
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Helpers
{
    public static class EditorHelper
    {
        public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled,
            params GUILayoutOption[] options)
        {
            GUI.backgroundColor = buttonColor;
            GUI.enabled = buttonEnabled;
            bool button = GUILayout.Button(buttonName, options);
            GUI.enabled = true;
            GUI.backgroundColor = Color.white;

            return button;
        }

        /// <summary>
        /// Create GUI button with name,
        /// color and make it enable or
        /// disable
        /// </summary>
        public static bool Button(string buttonName, Color buttonColor, bool buttonEnabled = true)
        {
            GUI.backgroundColor = buttonColor;
            GUI.enabled = buttonEnabled;
            bool button = GUILayout.Button(buttonName);
            GUI.enabled = true;
            GUI.backgroundColor = Color.white;

            return button;
        }
    }
}

#endif
