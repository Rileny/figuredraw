﻿#if UNITY_EDITOR
using EJaw.EditorClasses.UnityDatabase.Helpers;
using EJaw.EditorClasses.UnityDatabase.Layouts;
using EJaw.UnityDatabase;
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;
using UnityEditor;
using UnityEngine;

namespace EJaw.EditorClasses.UnityDatabase.Windows
{
    public abstract class BaseTableWindow<TTable, TRecord> : EditorWindow
        where TTable : ScriptableObjectTable, ITable<TRecord>, IEditorTable<TRecord>
        where TRecord : IRecord, IEditorRecord, new()
    {
        #region Fields

        protected TTable table = null;
        private Vector3 recordsScroll = new Vector3();
        private Vector3 tableScroll = new Vector3();
        private string newTableName = string.Empty;
        private IWindowLayout tableDataLayout = null;
        private IWindowLayout recordDataLayout = null;
        private const float VERTICAL_STEP = 15f;
        private const float HORIZONTAL_STEP = 15f;

        #endregion

        #region Properties

        private IWindowLayout TableDataLayout
        {
            get
            {
                if (tableDataLayout == null)
                {
                    tableDataLayout = CreateTableLayout();
                    if (tableDataLayout == null)
                        tableDataLayout = new HorizontalLayout();
                }
                return tableDataLayout;
            }
        }

        private IWindowLayout RecordDataLayout
        {
            get
            {
                if (recordDataLayout == null)
                {
                    recordDataLayout = CreateRecordLayout();
                    if (recordDataLayout == null)
                        recordDataLayout = new HorizontalLayout();
                }
                return recordDataLayout;
            }
        }

        #endregion

        #region EditorWindow events

        protected virtual void OnGUI()
        {
            SelectTable();
            GUILayout.Space(VERTICAL_STEP);
            DispayTableData();
            GUILayout.Space(VERTICAL_STEP);
            DisplayRecordsData();

            if (GUI.changed && table != null)
                EditorUtility.SetDirty(table);
        }

        protected virtual void OnFocus()
        {
            if (table == null)
                table = UDB.RuntimeInstance.GetTable<TRecord>() as TTable;
        }

        #endregion
        
        #region Functions

        protected virtual IWindowLayout CreateTableLayout()
        {
            return new HorizontalLayout();
        }

        protected virtual IWindowLayout CreateRecordLayout()
        {
            return new HorizontalLayout();
        }

        protected virtual void DispayTableData()
        {
            if (table == null) return;
            tableScroll = GUILayout.BeginScrollView(tableScroll);
            TableDataLayout.DrawTable(table);
            GUILayout.EndScrollView();
        }

        protected virtual void DisplayRecordsData()
        {
            if (table == null) return;
            recordsScroll = GUILayout.BeginScrollView(recordsScroll);
            RecordDataLayout.DrawRecord<TTable, TRecord>(table);
            GUILayout.EndScrollView();
        }

        protected virtual void SelectTable()
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(HORIZONTAL_STEP);
            EditorGUILayout.HelpBox("Select existing table or create new:", MessageType.None);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(HORIZONTAL_STEP);
            table = EditorGUILayout.ObjectField("Table", table, typeof (TTable), false) as TTable;
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            GUILayout.Space(HORIZONTAL_STEP);
            newTableName = EditorGUILayout.TextField("New table name", newTableName);
            if (EditorHelper.Button("Create", Color.green, !string.IsNullOrEmpty(newTableName)))
                UDB.Create<TTable>(newTableName);
            EditorGUILayout.EndHorizontal();
        }

        #endregion

    }
}

#endif
