﻿#if UNITY_EDITOR
using EJaw.UnityDatabase;
using EJaw.UnityDatabase.Records;
using EJaw.UnityDatabase.Tables;
using UnityEditor;

namespace EJaw.EditorClasses.UnityDatabase.Windows
{
    public sealed class TablesDatabaseWindow : BaseTableWindow<TablesDatabase, TableRecord>
    {
        protected override void OnFocus()
        {
            if (table == null)
                table = UDB.GetTablesDatabase();
        }

        protected override void DispayTableData()
        {
            //Nothing to display
        }

        protected override void SelectTable()
        {
            //Nothing to select
        }

        [MenuItem("Tools/EJaw/Database/Tables database")]
        public static void OpenTablesDatabaseWindow()
        {
            GetWindow<TablesDatabaseWindow>(false, "Tables database", true);
        }
    }
}

#endif
